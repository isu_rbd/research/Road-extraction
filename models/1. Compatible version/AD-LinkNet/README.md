## Considerations

Image size is a problem. I discovered that input sizes that are multiples of 2^5 work, which is why we use 608.


## Modifications

Lines 65-67:
- In ELayer constructor, cast parameters to nn.Linear to integer since "in python2 int/int=int, but in python3 int/int=float."
- Source: https://github.com/tengshaofeng/ResidualAttentionNetwork-pytorch/issues/3


General:
- UserWarning: nn.functional.sigmoid is deprecated. Use torch.sigmoid instead.

