#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import os
import os.path
from os import path
from datetime import datetime
import numpy as np
import torch
from torch.utils.data import DataLoader

sys.path.append(r'/work/LAS/zhuz-lab/road_extraction/AD-LinkNet')   # Nova, Pronto
sys.path.append(r'/vol/data/zhuz/rbatista/AD-LinkNet')              # Centipede
sys.path.append(r'/Users/ricardobatista/Box/Road extraction/models/2. Implementations/AD-LinkNet/AD-LinkNet') # Local

import modules.settings as set_mod
import modules.image as im_mod
from modules.RoadDataset import RoadDataset

MEAN, STD = set_mod.MEAN, set_mod.STD


print("Getting data objects started", datetime.now().strftime("%H:%M:%S"), flush = True) # Feedback

if path.exists(set_mod.PATH_LOADER_TRAIN):
    
    loader_train = torch.load(set_mod.PATH_LOADER_TRAIN)
    loader_val   = torch.load(set_mod.PATH_LOADER_VAL)
    
    print([len(dataset_train), len(dataset_val), len(dataset_test)])
    
else:
    
    # Get data split
    paths_im = im_mod.get_paths_im(set_mod.PATH_DATASET, set_mod.DATA_SPLIT, set_mod.GOOGLE_IMAGES)
    
    # Get Datasets
    dataset_train = RoadDataset(paths_im["train"], mean = MEAN, std = STD, augment = True,  # Change eventually
                                save = True, path_save = set_mod.PATH_DATASET_TRAIN)
    dataset_val   = RoadDataset(paths_im["val"], mean = MEAN, std = STD,
                                save = True, path_save = set_mod.PATH_DATASET_VAL)
    dataset_test  = RoadDataset(paths_im["test"], mean = MEAN, std = STD,
                                save = True, path_save = set_mod.PATH_DATASET_TEST)
    
    # Get Dataloaders
    loader_train = DataLoader(dataset_train, batch_size = set_mod.BATCH_SIZE_TRAIN, 
                              shuffle = True, num_workers = set_mod.NUM_WORKERS)
    loader_val   = DataLoader(dataset_val, batch_size = set_mod.BATCH_SIZE_VAL_TEST, 
                              num_workers = set_mod.NUM_WORKERS)
    loader_test  = DataLoader(dataset_test, batch_size = set_mod.BATCH_SIZE_VAL_TEST, 
                              num_workers = set_mod.NUM_WORKERS)

    # Save data
    torch.save(loader_train, set_mod.PATH_LOADER_TRAIN)
    torch.save(loader_val, set_mod.PATH_LOADER_VAL)
    torch.save(loader_test, set_mod.PATH_LOADER_TEST)
    
    
print("Getting data objects complete", datetime.now().strftime("%H:%M:%S"), flush = True) # Feedback


# In[2]:


# Batch visualization
im_mod.show_batch(loader_train, MEAN, STD, num_batches = 5)


# In[3]:


# Model

from models import ADLinkNet

model = ADLinkNet.ADLinkNet50()


# In[4]:


# Device

import torch.nn as nn

cuda = torch.cuda.is_available()
device = torch.device("cuda" if cuda else "cpu")
print("device:", device, flush = True)

if torch.cuda.device_count() > 1:
    print("Let's use", torch.cuda.device_count(), "GPUs!", flush = True)
    # dim = 0 [30, xxx] -> [10, ...], [10, ...], [10, ...] on 3 GPUs
    model = nn.DataParallel(model)

model.to(device)


# In[5]:


# Resume from checkpoint

import numpy as np

PATH_CHECKPOINT = set_mod.PATH_CHECKPOINT

if path.exists(PATH_CHECKPOINT):
    
    start_epoch = np.load("checkpoints/epoch.npy") + 1
    
    if cuda:
        checkpoint = torch.load(PATH_CHECKPOINT)
    else:
        # Load GPU model on CPU
        checkpoint = torch.load(PATH_CHECKPOINT,
                                map_location = lambda storage,
                                loc: storage)
    
    val_best = checkpoint['min_loss']
    model.load_state_dict(checkpoint['state_dict'])
    print("=> loaded checkpoint '{}' (trained for {} epochs)".format(PATH_CHECKPOINT, checkpoint['epoch']),
          flush = True)

else:
    start_epoch = 1
    val_best = 1

epoch_range = range(start_epoch, set_mod.NUM_EPOCHS)
print("starting epoch:", start_epoch, flush = True)


# In[ ]:


# Train

print("Training started", datetime.now().strftime("%H:%M:%S"), flush = True) # Feedback

import modules.losses as losses
import modules.utils  as utils

criterion = losses.DiceLoss()
criterion_val = losses.DiceLoss_vec()
optimizer = torch.optim.Adam(model.parameters(), lr = set_mod.LEARNING_RATE)

for epoch in epoch_range:
    
    # Train
    loss_train = []
    batch_num = 1
    for batch in loader_train:

        x_batch = batch['image'].to(device)
        y_batch = batch['mask'].to(device)

        y_pred = model(x_batch)
        loss = criterion(y_pred, y_batch)
        loss_train.append(loss.item()) # bookkeeping

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        batch_num += 1                 # bookkeeping
        if ((batch_num % 100) == 0):
            print("batch number (train):", batch_num, flush = True)

    loss_train = np.mean(loss_train)   # bookkeeping
    
    
    # Validation
    loss_val = []
    batch_num = 1
    with torch.no_grad():
        for batch in loader_val:

            x_batch = batch['image'].to(device)
            y_batch = batch['mask'].to(device)

            model.eval()

            y_pred = model(x_batch)     
            loss = criterion_val(y_pred, y_batch)
            loss_val = loss_val + loss.tolist() # bookkeeping
            
            batch_num += 1                      # bookkeeping
            if ((batch_num % 100) == 0):
                print("batch number (val):", batch_num, flush = True)
    
    loss_val = np.mean(loss_val)
    
    
    # Bookkeeping and feedback
    log_epoch = {"epoch" : epoch, 
                 "loss_train" : round(loss_train, 8),
                 "loss_val" : round(loss_val, 8)}
    
    utils.log([log_epoch], set_mod.PATH_LOG)
    
    print(log_epoch, flush = True)
    
    
    # Save checkpoint
    is_best  = bool(loss_val < val_best)
    val_best = min(loss_val, val_best)
    utils.save_checkpoint({
        'epoch': epoch,
        'state_dict': model.state_dict(),
        'min_loss': val_best}, 
        is_best)


# In[ ]:




