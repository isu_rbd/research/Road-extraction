import torch
import numpy as np
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms, utils
import random

import modules.settings as set_mod

IM_LEN = set_mod.IM_LEN


class RoadDataset(Dataset):
    
    def __init__(self, paths_image, norm = True, mean = None, std = None, 
                 augment = False, save = False, path_save = None):
        
        self.paths_image = paths_image
        self.norm = norm
        self.mean = mean
        self.std = std
        self.augment = augment
        self.save = save
        self.path_save = path_save
        
        
        # Basic transformations
        self.resize = Resize(IM_LEN)
        self.toTensor = ToTensor()
        if norm:
            self.normalize = transforms.Normalize(self.mean, self.std)
        
        # Data augmentation
        self.jitter      = transforms.ColorJitter(brightness = 0.4, contrast = 0.4, saturation = 0.4, hue = 0.4)
        self.horizontal  = HorizontalFlip()
        self.vertical    = VerticalFlip()
        self.degrees     = Degrees(5)
        # self.scale       = Scale(0)
        self.translateH  = TranslateHorizontal(0.05)
        self.translateV  = TranslateVertical(0.05)
        
        
        if save:
            torch.save(self, path_save)
    
    
    def __len__(self):
        return len(self.paths_image)
    
    
    def set_paths_image(self, paths_image):
        self.paths_image = paths_image
    
    
    def get_paths_image(self):
        return self.paths_image
    
    
    def get_im_path(self, idx):
        return self.paths_image[idx]
        
        
    def get_mean_and_std(self):
        return self.mean, self.std
        
        
    def __getitem__(self, idx):
        
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        
        # Get PIL images
        path_image = self.paths_image[idx]
        image = Image.open(path_image)
        
        if set_mod.GOOGLE_IMAGES:
            
            mask  = Image.open(path_image.replace("/im/", "/gt/"))
            
            # Transforms on PIL
            ## Basic
            ### Resize (so image and mask are a multiple of 32 and can thus work with AD-LinkNet code)
            sample = self.resize({'image' : image, 'mask' : mask})

            ### Change mask to matrix of zeros (black, background) and ones (white, road)
            sample['mask'] = sample['mask'].convert('L')                               # grayscale 
            sample['mask'] = sample['mask'].point(lambda x: 1 if x < 128 else 0, '1')  # road: 1 (white), background: 0 (black)
            
        else:
            
            mask  = Image.open(path_image.replace("sat.jpg", "mask.png"))
            
            mask = mask.convert('L')
            mask = mask.point(lambda x: 0 if x < 128 else 1, '1')
            sample = {'image' : image, 'mask' : mask}
        
        
        ## Augmentation
        if self.augment*random.randint(0, 1):
            if random.randint(0, 1):
                sample['image'] = self.jitter(sample['image'])
            if random.randint(0, 1):
                sample = self.horizontal(sample)
            if random.randint(0, 1):
                sample = self.vertical(sample)
            if random.randint(0, 1):
                sample = self.degrees(sample)
            # if random.randint(0, 1):
            #     sample = self.scale(sample)
            if random.randint(0, 1):
                sample = self.translateH(sample)
            if random.randint(0, 1):
                sample = self.translateV(sample)
                
        
        # Convert PIL to torch.*Tensor
        sample = self.toTensor(sample)
                    
        
        # Transforms on torch.*Tensor
        ## Normalize
        if self.norm:
            sample['image'] = self.normalize(sample['image'])
        
        return sample

    
class Resize(object):
    
    def __init__(self, output_size):
        self.output_size = output_size
        
    def __call__(self, sample):
        image, mask = sample['image'], sample['mask']
        
        image = image.resize((self.output_size, self.output_size)) # Image.BICUBIC is PIL default
        mask  = mask.resize((self.output_size, self.output_size))
        
        return {'image': image, 'mask': mask}

    
class HorizontalFlip(object):
    
    def __call__(self, sample):
        image, mask = sample['image'], sample['mask']
        
        tsfm = transforms.RandomHorizontalFlip(p = 1)   # flip with probability 1
        
        return {'image': tsfm(image), 'mask': tsfm(mask)}

    
class VerticalFlip(object):
    
    def __call__(self, sample):
        image, mask = sample['image'], sample['mask']
        
        tsfm = transforms.RandomVerticalFlip(p = 1)   # flip with probability 1
        
        return {'image': tsfm(image), 'mask': tsfm(mask)}

    
class Degrees(object):
    
    def __init__(self, degrees):
        self.degrees = degrees
        
    def __call__ (self, sample):
        image, mask = sample['image'], sample['mask']
        
        degrees = random.uniform(-self.degrees, self.degrees)
        
        image = transforms.functional.affine(image, angle = degrees, translate = (0, 0), 
                                             scale = 1, shear = 0)
        mask  = transforms.functional.affine(mask, angle = degrees, translate = (0, 0), 
                                             scale = 1, shear = 0)
        
        return {'image': image, 'mask': mask}

    
class Scale(object):
    
    def __init__(self, scale):
        self.scale = scale
        
    def __call__ (self, sample):
        image, mask = sample['image'], sample['mask']
        
        scale   = random.uniform(1 - self.scale, 1 + self.scale)
        
        image = transforms.functional.affine(image, angle = 0, translate = (0, 0), 
                                             scale = scale, shear = 0)
        mask  = transforms.functional.affine(mask, angle = 0, translate = (0, 0), 
                                             scale = scale, shear = 0)
        
        return {'image': image, 'mask': mask}

    
class TranslateHorizontal(object):
    
    def __init__(self, translate):
        self.translate = translate
        
    def __call__ (self, sample):
        image, mask = sample['image'], sample['mask']
        
        translate = random.uniform(-IM_LEN*self.translate, IM_LEN*self.translate)
        
        image = transforms.functional.affine(image, angle = 0, translate = (translate, 0), 
                                             scale = 1, shear = 0)
        mask  = transforms.functional.affine(mask, angle = 0, translate = (translate, 0), 
                                             scale = 1, shear = 0)
        
        return {'image': image, 'mask': mask}

    
class TranslateVertical(object):
    
    def __init__(self, translate):
        self.translate = translate
        
    def __call__ (self, sample):
        image, mask = sample['image'], sample['mask']
        
        translate = random.uniform(-IM_LEN*self.translate, IM_LEN*self.translate)
        
        image = transforms.functional.affine(image, angle = 0, translate = (0, translate), 
                                             scale = 1, shear = 0)
        mask  = transforms.functional.affine(mask, angle = 0, translate = (0, translate), 
                                             scale = 1, shear = 0)
        
        return {'image': image, 'mask': mask}
    

class ToTensor(object):
    
    def __call__(self, sample):
        image = np.asarray(sample['image'], dtype = np.float32)
        mask  = np.asarray(sample['mask'], dtype = np.float32)

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        image = image.transpose((2, 0, 1))
        
        # add a dimension to mask so it matches standard model output
        x = np.zeros([1, IM_LEN, IM_LEN], dtype = np.float32)
        x[0, :, :] = mask

        return {'image': torch.FloatTensor(image), 'mask' : torch.FloatTensor(x)}

    
    
"""
General 
# https://pytorch.org/tutorials/recipes/recipes/custom_dataset_transforms_loader.html
# https://pytorch.org/tutorials/beginner/data_loading_tutorial.html

Normalization
# https://stackoverflow.com/questions/49444262/normalize-data-before-or-after-split-of-training-and-testing-data#:~:text=Feature%20normalization%20(or%20data%20standardization,and%20dividing%20by%20the%20variance.&text=In%20this%20way%2C%20we%20can,to%20new%2C%20unseen%20data%20points.

## Normalize after other transforms
# https://discuss.pytorch.org/t/color-augmentation-before-of-after-normalisation-for-resnet/22537

Bicubic
# https://codesuche.com/python-examples/PIL.Image.BICUBIC/?ipage=3
"""
