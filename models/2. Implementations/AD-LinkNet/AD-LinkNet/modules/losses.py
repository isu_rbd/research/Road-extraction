import torch
import numpy as np
from torch import nn
from torch import einsum

    
class DiceLoss(nn.Module):
    def __init__(self):
        super(DiceLoss, self).__init__()
    
    def forward(self, y_true, y_pred, smooth=1):
        intersection = torch.sum(y_true * y_pred, dim=(1,2,3))
        union = torch.sum(y_true, dim=(1,2,3)) + torch.sum(y_pred, dim=(1,2,3))
        dice_coef = torch.mean( (2. * intersection + smooth) / (union + smooth), dim=0)
        loss = 1 - dice_coef
        return loss
    

    """
    From Charlie

    def dice_coef(y_true, y_pred, smooth=1):
        intersection = K.sum(y_true * y_pred, axis=[1,2,3])
        union = K.sum(y_true, axis=[1,2,3]) + K.sum(y_pred, axis=[1,2,3])
        return K.mean( (2. * intersection + smooth) / (union + smooth), axis=0)

    def dice_loss(y_true, y_pred):
        loss = 1 - dice_coef(y_true, y_pred)
        return loss
        
    """

    
class DiceLoss_vec(nn.Module):
    def __init__(self):
        super(DiceLoss_vec, self).__init__()
    
    def forward(self, y_true, y_pred, smooth=1):
        intersection = torch.sum(y_true * y_pred, dim=(1,2,3))
        union = torch.sum(y_true, dim=(1,2,3)) + torch.sum(y_pred, dim=(1,2,3))
        dice_coef = (2. * intersection + smooth) / (union + smooth)
        loss = 1 - dice_coef
        return loss


"""
class DiceLoss(nn.Module):
    def __init__(self, smooth=1e-5):

        super(DiceLoss, self).__init__()

        self.smooth = smooth

    def forward(self, pred, gt):
        
        intersection: Tensor = einsum("bcwh,bcwh->bc", pred, gt)             # (batch size, num of classes, w, h)
        union: Tensor = (einsum("bcwh->bc", pred) + einsum("bcwh->bc", gt))
        divided: Tensor = 1 - (2 * intersection + 1e-10) / (union + 1e-10)

        loss = divided.mean()

        return loss
        
"""