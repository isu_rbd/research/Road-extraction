import numpy as np

#### Parameters ####

DATA_SPLIT = {"train" : 0.64, "val" : 0.16, "test" : 0.2}

NUM_EPOCHS = 100

LEARNING_RATE = 1e-6  # 1e-6 is best so far

NUM_WORKERS = 2*8  # For dataloader. The multiple should be in [2, 8], and 2 is for number of GPUs.  

SIZE_PRED = 40

GOOGLE_IMAGES = True

if GOOGLE_IMAGES:
    
    IM_LEN = 608
    
    BATCH_SIZE_TRAIN    = 32   # 2, 8, 32   /// Note GPU runs out of memory at 64
    BATCH_SIZE_VAL_TEST = 32
    
    MEAN = np.array([85.78610137, 87.65715428, 71.23262791])  # Calculated on random training dataset
    STD  = np.array([37.76451206, 31.22531042, 30.56542678])
    
else:

    IM_LEN = 1024
    
    BATCH_SIZE_TRAIN    = 8   # 2, 8, 32   /// Note GPU runs out of memory at 16
    BATCH_SIZE_VAL_TEST = 8
    
    MEAN = np.array([104.35230986,  97.50908866,  73.38092155])  # Calculated on random training dataset
    STD  = np.array([39.75799935, 32.37035041, 31.51429171])
    
    
#### Paths ####

HPC = False

if HPC:
    
    # General
    PATH_ROOT = "/work/LAS/zhuz-lab/road_extraction/"       # Nova, Pronto
    PATH_IMPLEMENTATION = PATH_ROOT + "AD-LinkNet/"         # Nova, Pronto

    if GOOGLE_IMAGES:
        # Data
        PATH_DATASET = PATH_ROOT + "Dataset/dataset"            # Nova, Pronto
        # PATH_DATASET = PATH_ROOT + "dataset_tiny"             # Nova
    else:
        PATH_DATASET = PATH_ROOT + "dataset_cvpr"

else:
    
    # General
    PATH_ROOT = "/Users/ricardobatista/Box/Road extraction/"
    PATH_IMPLEMENTATION = PATH_ROOT + "models/2. Implementations/AD-LinkNet/AD-LinkNet/"
    
    # Data
    if GOOGLE_IMAGES:
        PATH_DATASET = PATH_ROOT + "z. backend/5. Other datasets/dataset_tiny/dataset_tiny"
    else:
        PATH_DATASET = PATH_ROOT + "z. backend/5. Other datasets/dataset_cvpr_tiny"
    
    # Testing and prediction
    PATH_LOAD = PATH_ROOT + "z. backend/6. HPC/2020.09.07/Pronto/AD-LinkNet/01/2. HPC download/AD-LinkNet/"
    # PATH_LOAD = PATH_ROOT + "z. backend/6. HPC/2020.09.02/Nova/AD-LinkNet/03/2. HPC download/AD-LinkNet/"
    PATH_DATASET_TEST_LOAD = PATH_LOAD + "data/dataset_test.pth"
    # PATH_DATASET_TEST_LOAD = PATH_IMPLEMENTATION + "data/dataset_test.pth"
    PATH_CHECKPOINT_LOAD = PATH_LOAD + "train/checkpoints/checkpoint.pth.tar"
    # PATH_CHECKPOINT_LOAD = PATH_IMPLEMENTATION + "train/checkpoints/checkpoint.pth.tar"
    


# Data objects

PATH_DATASET_TRAIN = PATH_IMPLEMENTATION + "data/dataset_train.pth"
PATH_DATASET_VAL   = PATH_IMPLEMENTATION + "data/dataset_val.pth"
PATH_DATASET_TEST  = PATH_IMPLEMENTATION + "data/dataset_test.pth"

PATH_LOADER_TRAIN = PATH_IMPLEMENTATION + "data/dataloader_train.pth"
PATH_LOADER_VAL   = PATH_IMPLEMENTATION + "data/dataloader_val.pth"
PATH_LOADER_TEST  = PATH_IMPLEMENTATION + "data/dataloader_test.pth"


# Training

PATH_EPOCH      = PATH_IMPLEMENTATION + "train/checkpoints/epoch.npy"
PATH_CHECKPOINT = PATH_IMPLEMENTATION + "train/checkpoints/checkpoint.pth.tar"
PATH_LOG        = PATH_IMPLEMENTATION + "train/logs/logs.csv"


# The remaining settings are in the .ipynb files:
#  - Set the model
#  - Set the loss (default = DiceLoss)
#  - Set the optimizer (default = Adam) and learning rate
