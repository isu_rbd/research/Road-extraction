# Evaluation module

import cv2
import torch
import ntpath
import numpy as np
import collections #delete

import modules.settings as set_mod
import modules.image    as im_mod

IM_LEN    = set_mod.IM_LEN

# Need to clean up
def get_predicted(dataset_pred, model, criterion):
    
    for i in range(len(dataset_pred)):
        sample = dataset_pred[i]
        
        image, mask = sample['image'], sample['mask']
        
        # add a dimension to mask so it matches standard model output
        x = np.zeros([1, 3, IM_LEN, IM_LEN])
        y = np.zeros([1, 1, IM_LEN, IM_LEN])
        
        x[0, :, :, :] = image.numpy()
        y[0, :, :, :] = mask.numpy()
        
        unique, counts = np.unique(y, return_counts=True)
        print(dict(zip(unique, counts)))
        
        x_tensor = torch.FloatTensor(x)
        y_tensor = torch.FloatTensor(y)
        
        # image, mask = image.numpy(), mask.numpy()
        
        # x_tensor = torch.zeros([1, 3, IM_LEN, IM_LEN], dtype = torch.int32)
        # y_tensor = torch.zeros([1, 1, IM_LEN, IM_LEN], dtype = torch.int32)
        
        # x_tensor[0, :, :, :] = image
        # y_tensor[0, :, :, :] = mask
        
        # Get prediction
        yhat_tensor = model(x_tensor)
        print(y_tensor)
        print(yhat_tensor)
        
        ## Get Dice loss and score
        loss_pred  = float(criterion(yhat_tensor, y_tensor).detach().numpy())
        dice_score = 1 - loss_pred

        # Transform to street map
        yhat = np.round_(yhat_tensor.detach().numpy()[0, 0, :, :])
        yhat_pic = np.zeros([IM_LEN, IM_LEN, 3])

        ## Note: the tuple (255, 255, 255) represents white.
        yhat_pic[:, :, 0] = 255            # Blue
        yhat_pic[:, :, 1] = (1 - yhat)*255 # Green
        yhat_pic[:, :, 2] = (1 - yhat)*255 # Red

        ## Resize it
        yhat_pic_final = cv2.resize(yhat_pic, (600, 600), interpolation = cv2.INTER_AREA)


        # Produce image

        ## Load original im and gt
        path_pred_im = dataset_pred.get_im_path(i)
        pred_im = cv2.imread(path_pred_im)
        pred_gt = cv2.imread(path_pred_im.replace("/im/", "/gt/"))

        ## Stitch them together
        pred_output = np.concatenate((pred_im, pred_gt, yhat_pic_final), axis = 1)

        ## Annotate them
        pred_output = cv2.putText(pred_output, 'Ground truth', (800, 50),
                                  fontFace = 0, fontScale = 1, color = (0, 0, 0), 
                                  thickness = 2)
        pred_output = cv2.putText(pred_output, 
                                  'Prediction (Dice: ' + str(round(dice_score, 3)) + ')', (1300, 50),
                                  fontFace = 0, fontScale = 1, color = (0, 0, 0),
                                  thickness = 2)

        print("predictions/" + ntpath.basename(path_pred_im))
        cv2.imwrite("predictions/" + ntpath.basename(path_pred_im), pred_output)
        
        
    
"""
def get_predicted(path_pred_im, model, criterion):
    
    path_pred_gt = path_pred_im.replace("/im/", "/gt/")
    
    x_tensor = torch.FloatTensor(im_mod.trans_im(path_pred_im))
    y_tensor = torch.FloatTensor(im_mod.trans_gt(path_pred_gt))
    
    # Get prediction
    yhat_tensor = model(x_tensor)
    
    ## Get Dice loss and score
    loss_pred  = float(criterion(yhat_tensor, y_tensor).detach().numpy())
    dice_score = -(loss_pred - 1)
    
    # Transform to street map
    yhat = np.round_(yhat_tensor.detach().numpy()[0, 0, :, :])
    yhat_pic = np.zeros([IM_LEN, IM_LEN, 3])

    ## Note: the tuple (255, 255, 255) represents white.
    yhat_pic[:, :, 0] = 255            # Blue
    yhat_pic[:, :, 1] = (1 - yhat)*255 # Green
    yhat_pic[:, :, 2] = (1 - yhat)*255 # Red

    ## Resize it
    yhat_pic_final = cv2.resize(yhat_pic, (600, 600), interpolation = cv2.INTER_AREA)
    
    
    # Produce image
    
    ## Load original im and gt
    pred_im = cv2.imread(path_pred_im)
    pred_gt = cv2.imread(path_pred_gt)
    
    ## Stitch them together
    pred_output = np.concatenate((pred_im, pred_gt, yhat_pic_final), axis = 1)
    
    ## Annotate them
    pred_output = cv2.putText(pred_output, 'Ground truth', (800, 50),
                              fontFace = 0, fontScale = 1, color = (0, 0, 0), 
                              thickness = 2)
    pred_output = cv2.putText(pred_output, 
                              'Prediction (Dice: ' + str(round(dice_score, 3)) + ')', (1300, 50),
                              fontFace = 0, fontScale = 1, color = (0, 0, 0),
                              thickness = 2)
    
    print("predictions/" + ntpath.basename(path_pred_im))
    cv2.imwrite("predictions/" + ntpath.basename(path_pred_im), pred_output)

"""
