# Image module

import os
from PIL import Image
from matplotlib import cm
import numpy as np
import random
import glob
import torch
from torchvision import transforms, utils

import modules.settings as set_mod

IM_LEN    = set_mod.IM_LEN


# Establish the train, val, and test sets. Note we create these sets
# by splitting the images FOR EACH STATE according to the split indicated
# by "data_split", as opposed to just taking the entire list of 30k images
# and applying a single split on said list. We do this because sampling 
# proportionately from each state ensures the train, val, and test sets
# are representative of the population.
def get_paths_im(path, data_split, google_images):
    
    train, val, test = data_split["train"], data_split["val"], data_split["test"]
    
    paths_im = {"train" : [], "val" : [], "test" : []}
    
    if set_mod.GOOGLE_IMAGES:
    
        paths_state = [f.path for f in os.scandir(path) if f.is_dir() if not f.name.startswith('.')]

        for path_state in paths_state:

            paths_state_im = glob.glob(path_state + "/im/*.png")
            random.shuffle(paths_state_im)
            num_im   = len(paths_state_im)
            breaks   = [int(num_im*train), int(num_im*(train + val))]
            train_temp, val_temp, test_temp = np.split(paths_state_im, breaks)
            
            paths_im["train"].extend(train_temp.tolist())
            paths_im["val"].extend(val_temp.tolist())
            paths_im["test"].extend(test_temp.tolist())
            
    else:
        
        paths_im_temp = glob.glob(path + "/*.jpg")
        
        random.shuffle(paths_im_temp)
        num_im   = len(paths_im_temp)
        breaks   = [int(num_im*train), int(num_im*(train + val))]
        train_temp, val_temp, test_temp = np.split(paths_im_temp, breaks)
        
        paths_im["train"].extend(train_temp.tolist())
        paths_im["val"].extend(val_temp.tolist())
        paths_im["test"].extend(test_temp.tolist())
        
    
    return paths_im


def get_mean_and_std(paths_image):
    image_num = len(paths_image)
    x = np.zeros([image_num, IM_LEN, IM_LEN, 3])
    for i in range(image_num):
        image = Image.open(paths_image[i])
        image = image.resize((IM_LEN, IM_LEN)) # Image.BICUBIC is PIL default
        x[i, :, :, :] = np.asarray(image) 
        
    # swap color axis because
    # numpy image: B x H x W x C
    # torch image: B x C X H X W
    x = x.transpose((0, 3, 1, 2))
    
    means  = np.mean(x, axis = (0, 2, 3))    # all axes except the channel axis
    stdevs = np.std(x, axis = (0, 2, 3))
    
    return means, stdevs


def show_batch(dataloader, mean, std, num_batches = 4, num_per_batch = 3):
    
    image_output = None
    
    for i_batch, sample_batched in enumerate(dataloader):
        
        images_batch = sample_batched['image'][0:num_per_batch, :, :, :]
        mask_batch   = sample_batched['mask'][0:num_per_batch, :, :, :]
        
        
        # De-normalize image
        mean_tensor = torch.zeros([1, 3, 1, 1])
        std_tensor  = torch.zeros([1, 3, 1, 1])
        
        mean_tensor[0, :, 0, 0] = torch.from_numpy(mean)
        std_tensor[0, :, 0, 0]  = torch.from_numpy(std)
        
        images_batch = images_batch*std_tensor + mean_tensor
        
        
        # Transform mask into black and white
        mask_batch = mask_batch*255
        
        
        # Make into grid
        grid_image_tensor = utils.make_grid(images_batch, nrow = 1)
        grid_mask_tensor  = utils.make_grid(mask_batch, nrow = 1)
        
        grid_image = grid_image_tensor.numpy().transpose((1, 2, 0))
        grid_mask  = grid_mask_tensor.numpy().transpose((1, 2, 0))
        
        
        ## Stitch them together
        grid_output = np.concatenate((grid_image, grid_mask), axis = 1)
        
        grid_output = Image.fromarray(grid_output.astype(np.uint8))
        grid_output.save("batch_viz/batch_viz_{}.png".format(i_batch))
        
        if(i_batch == num_batches):
            break
    
    
"""
https://stackoverflow.com/questions/10965417/how-to-convert-a-numpy-array-to-pil-image-applying-matplotlib-colormap
"""
