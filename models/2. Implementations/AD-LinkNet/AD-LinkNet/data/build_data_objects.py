#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import os
import os.path
from os import path
from datetime import datetime
import torch
from torch.utils.data import DataLoader

sys.path.append(r'/work/LAS/zhuz-lab/road_extraction/AD-LinkNet')   # Nova, Pronto
sys.path.append(r'/vol/data/zhuz/rbatista/AD-LinkNet')              # Centipede
sys.path.append(r'/Users/ricardobatista/Box/Road extraction/models/2. Implementations/AD-LinkNet/AD-LinkNet') # Local

import modules.settings as set_mod
import modules.image as im_mod
from modules.RoadDataset import RoadDataset


print("Building data objects started", datetime.now().strftime("%H:%M:%S"), flush = True) # Feedback

# Get data split
paths_im = im_mod.get_paths_im(set_mod.PATH_DATASET, set_mod.DATA_SPLIT, set_mod.GOOGLE_IMAGES)

# Get channel-specific mean and stdev for the training dataset (for normalization)
mean, std = im_mod.get_mean_and_std(paths_im["train"])

print("mean")
print(mean)
print("std")
print(std)

# Get Datasets
dataset_train = RoadDataset(paths_im["train"], 
                            norm = True, mean = mean, std = std, augment = True,
                            save = True, path_save = set_mod.PATH_DATASET_TRAIN)
dataset_val   = RoadDataset(paths_im["val"], 
                            norm = True, mean = mean, std = std,
                            save = True, path_save = set_mod.PATH_DATASET_VAL)
dataset_test  = RoadDataset(paths_im["test"],
                            norm = True, mean = mean, std = std,
                            save = True, path_save = set_mod.PATH_DATASET_TEST)

# Get Dataloaders
loader_train = DataLoader(dataset_train, batch_size = set_mod.BATCH_SIZE_TRAIN, 
                          shuffle = True, num_workers = set_mod.NUM_WORKERS)
loader_val   = DataLoader(dataset_val, batch_size = set_mod.BATCH_SIZE_VAL_TEST, 
                          num_workers = set_mod.NUM_WORKERS)
loader_test  = DataLoader(dataset_test, batch_size = set_mod.BATCH_SIZE_VAL_TEST, 
                          num_workers = set_mod.NUM_WORKERS)

# Save data
torch.save(loader_train, set_mod.PATH_LOADER_TRAIN)
torch.save(loader_val, set_mod.PATH_LOADER_VAL)
torch.save(loader_test, set_mod.PATH_LOADER_TEST)

print("Building data objects complete", datetime.now().strftime("%H:%M:%S"), flush = True) # Feedback


# In[ ]:




