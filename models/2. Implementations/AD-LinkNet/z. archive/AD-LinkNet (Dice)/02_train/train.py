#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Load dataset

import sys
import os
import os.path
from os import path
from datetime import datetime
import torch

sys.path.append(r'/work/LAS/zhuz-lab/road_extraction/AD-LinkNet')
sys.path.append(r'/vol/data/zhuz/rbatista/AD-LinkNet')
sys.path.append(r'/Users/ricardobatista/Box/Road extraction/models/2. Implementations/AD-LinkNet/AD-LinkNet')

import modules.settings as set_mod


print("Dataloading started", datetime.now().strftime("%H:%M:%S"), flush = True) # Feedback
    
if (path.exists(set_mod.PATH_LOADER_TRAIN) and path.exists(set_mod.PATH_LOADER_VAL)):
    loader_train = torch.load(set_mod.PATH_LOADER_TRAIN)
    loader_val   = torch.load(set_mod.PATH_LOADER_VAL)
else:
    sys.exit("Dataloaders not found.")

print("Dataloading completed", datetime.now().strftime("%H:%M:%S"), flush = True) # Feedback


# In[ ]:


# Model

from models import ADLinkNet

model = ADLinkNet.ADLinkNet50()


# In[ ]:


# Device

import torch.nn as nn

cuda = torch.cuda.is_available()
device = torch.device("cuda" if cuda else "cpu")
print("device:", device, flush = True)

if torch.cuda.device_count() > 1:
    print("Let's use", torch.cuda.device_count(), "GPUs!", flush = True)
    # dim = 0 [30, xxx] -> [10, ...], [10, ...], [10, ...] on 3 GPUs
    model = nn.DataParallel(model)

model.to(device)


# In[ ]:


# Resume from checkpoint

import numpy as np

if path.exists(set_mod.PATH_CHECKPOINT):
    
    start_epoch = np.load("checkpoints/epoch.npy") + 1
    
    if cuda:
        checkpoint = torch.load(set_mod.PATH_CHECKPOINT)
    else:
        # Load GPU model on CPU
        checkpoint = torch.load(set_mod.PATH_CHECKPOINT,
                                map_location = lambda storage,
                                loc: storage)
    
    val_best = checkpoint['min_loss']
    model.load_state_dict(checkpoint['state_dict'])
    print("=> loaded checkpoint '{}' (trained for {} epochs)".format(set_mod.PATH_CHECKPOINT, checkpoint['epoch']),
          flush = True)

else:
    start_epoch = 1
    val_best = 1

epoch_range = range(start_epoch, set_mod.NUM_EPOCHS)
print("starting epoch:", start_epoch, flush = True)


# In[ ]:


# Train

print("Training started", datetime.now().strftime("%H:%M:%S"), flush = True) # Feedback

import modules.losses as losses
import modules.utils  as utils

criterion     = losses.DiceLoss()
criterion_val = losses.DiceLossVec()
optimizer = torch.optim.Adam(model.parameters(), lr = 1e-4)

for epoch in epoch_range:
    
    # Train
    loss_train = []
    batch_num = 1
    for x_batch, y_batch in loader_train:

        x_batch = x_batch.to(device)
        y_batch = y_batch.to(device)

        y_pred = model(x_batch)       
        loss = criterion(y_pred, y_batch)
        loss_train.append(loss.item()) # bookkeeping

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        batch_num += 1                # bookkeeping
        if ((batch_num % 250) == 0):
            print("batch number (train):", batch_num, flush = True)

    loss_train = np.mean(loss_train) # bookkeeping
    
    
    # Validation
    loss_val = []
    batch_num = 1
    with torch.no_grad():
        for x_val, y_val in loader_val:

            x_val = x_val.to(device)
            y_val = y_val.to(device)

            model.eval()

            yhat = model(x_val)
            loss_val = loss_val + criterion_val(yhat, y_val).tolist()
            
            batch_num += 1                # bookkeeping
            if ((batch_num % 250) == 0):
                print("batch number (val):", batch_num, flush = True)
    
    loss_val = np.mean(loss_val) 
    
    
    # Bookkeeping and feedback
    log_epoch = {"epoch" : epoch, 
                 "loss_train" : round(loss_train, 8),
                 "loss_val" : round(loss_val, 8)}
    
    utils.log([log_epoch], set_mod.PATH_LOG)
    
    print(log_epoch, flush = True)
    
    
    # Save checkpoint
    is_best  = bool(loss_val < val_best)
    val_best = min(loss_val, val_best)
    utils.save_checkpoint({
        'epoch': epoch,
        'state_dict': model.state_dict(),
        'min_loss': val_best}, 
        is_best)

