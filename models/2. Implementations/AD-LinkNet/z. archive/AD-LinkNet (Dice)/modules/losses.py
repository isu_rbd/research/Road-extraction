import torch
from torch import nn
from torch import einsum


"""
Dice: cobbled together from sources below
Copy from: https://github.com/LIVIAETS/surface-loss/blob/108bd9892adca476e6cdf424124bc6268707498e/losses.py#L29
paper: https://arxiv.org/pdf/1707.03237.pdf
tf code: https://github.com/NifTK/NiftyNet/blob/dev/niftynet/layer/loss_segmentation.py#L279
"""
class DiceLoss(nn.Module):
    def __init__(self, smooth=1e-5):

        super(DiceLoss, self).__init__()

        self.smooth = smooth

    def forward(self, pred, gt):
        
        intersection: Tensor = einsum("bcwh,bcwh->bc", pred, gt)             # (batch size, num of classes, w, h)
        union: Tensor = (einsum("bcwh->bc", pred) + einsum("bcwh->bc", gt))
        divided: Tensor = 1 - (2 * intersection + 1e-10) / (union + 1e-10)

        loss = divided.mean()
        
        return loss

    
"""
Dice: cobbled together from sources above and
https://medium.com/analytics-vidhya/pytorch-implementation-of-semantic-segmentation-for-single-class-from-scratch-81f96643c98c
"""
class DiceLossVec(nn.Module):
    def __init__(self, smooth=1e-5):

        super(DiceLossVec, self).__init__()

        self.smooth = smooth

    def forward(self, pred, gt):
        
        intersection: Tensor = einsum("bcwh,bcwh->bc", pred, gt)             # (batch size,c,w,h)
        union: Tensor = (einsum("bcwh->bc", pred) + einsum("bcwh->bc", gt))

        divided: Tensor = 1 - (2 * intersection + 1e-10) / (union + 1e-10)

        loss = divided
        
        return loss
