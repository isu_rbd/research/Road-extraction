#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Build dataset

import sys
import os
import os.path
from os import path
from datetime import datetime
import pickle
import torch

sys.path.append(r'/work/LAS/zhuz-lab/road_extraction/AD-LinkNet')
sys.path.append(r'/Users/ricardobatista/Box/Road extraction/models/2. Implementations/AD-LinkNet/AD-LinkNet')

import modules.settings as set_mod
import modules.image as im_mod

print("Build started", datetime.now().strftime("%H:%M:%S"), flush = True) # Feedback

paths_im = im_mod.get_paths_im(set_mod.PATH_DATASET, set_mod.DATA_SPLIT)
num_train, num_val, num_test = len(paths_im["train"]), len(paths_im["val"]), len(paths_im["test"])

loader_train = im_mod.get_loader(paths_im["train"], set_mod.BATCH_SIZE)
loader_val   = im_mod.get_loader(paths_im["val"], set_mod.BATCH_SIZE)
loader_test  = im_mod.get_loader(paths_im["test"], set_mod.BATCH_SIZE)

torch.save(loader_train, set_mod.PATH_LOADER_TRAIN)
torch.save(loader_val, set_mod.PATH_LOADER_VAL)
torch.save(loader_test, set_mod.PATH_LOADER_TEST)

with open(set_mod.PATH_TEST_IM, 'wb') as f:
    pickle.dump(paths_im["test"], f)

print("Build completed", datetime.now().strftime("%H:%M:%S"), flush = True) # Feedback

