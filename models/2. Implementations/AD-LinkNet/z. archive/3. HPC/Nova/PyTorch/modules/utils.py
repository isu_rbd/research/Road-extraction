import csv

def log(data_list, filename):
    
    csv_columns = sorted(data_list[0].keys())
    try:
        with open(filename, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames = csv_columns)
            writer.writeheader()
            for data in data_list:
                writer.writerow(data)
    except IOError:
        print("I/O error")
