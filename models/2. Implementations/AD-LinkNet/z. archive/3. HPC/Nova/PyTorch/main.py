#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import modules.settings as set_mod
import modules.image    as im_mod


# In[ ]:


# Load dataset

paths_im = im_mod.get_paths_im(set_mod.PATH_DATASET, set_mod.data_split)
num_train, num_val, num_test = len(paths_im["train"]), len(paths_im["val"]), len(paths_im["test"])

loader_train = im_mod.get_loader(paths_im["train"], set_mod.BATCH_SIZE)
loader_val   = im_mod.get_loader(paths_im["val"],   num_val)


# In[ ]:


# Model

from models import ADLinkNet
model = ADLinkNet.ADLinkNet50()


# In[ ]:


# Train

import torch
from modules import losses

device = 'cuda' if torch.cuda.is_available() else 'cpu'
criterion = losses.DiceLoss()
optimizer = torch.optim.Adam(model.parameters(), lr = 1e-4)
bookkeeping = []

for epoch in range(set_mod.NUM_EPOCHS):
    
    loss_train_acc = 0
    loss_val       = 0
    
    
    # Train
    for x_batch, y_batch in loader_train:

        x_batch = x_batch.to(device)
        y_batch = y_batch.to(device)

        y_pred = model(x_batch)
        loss = criterion(y_pred, y_batch)
        loss_train_acc += float(loss.detach().numpy()) # bookkeeping

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    loss_train = loss_train_acc / len(loader_train) # bookkeeping
    
    
    # Validation
    with torch.no_grad():
        for x_val, y_val in loader_val:

            x_val = x_val.to(device)
            y_val = y_val.to(device)

            model.eval()

            yhat = model(x_val)
            loss_val = criterion(y_val, yhat).item()

            
    # Bookkeeping and feedback
    log_epoch = {"epoch" : epoch, 
                 "loss_train" : round(loss_train, 8),
                 "loss_val" : round(loss_val, 8)}
    
    bookkeeping.append(log_epoch)
    
    print(log_epoch)


# In[ ]:


# Save weights

torch.save(model.state_dict(), "weights/model_state_dict.pt")


# In[ ]:


# Test

loss_test = 0
loader_test = im_mod.get_loader(paths_im["test"], num_test)

with torch.no_grad():
    for x_test, y_test in loader_test:

        x_test = x_test.to(device)
        y_test = y_test.to(device)

        model.eval()

        yhat = model(x_test)
        loss_test = criterion(y_test, yhat).item()

print(loss_test)


# In[ ]:


# Save feedback

import modules.utils as utils

## Bookkeeping
utils.log(bookkeeping, "logs/logs.csv")


## Summary
log_last_entry = bookkeeping[-1]
summary = [{'Epocs' : set_mod.NUM_EPOCHS,
            'num_train'  : num_train,'num_val' : num_val, 'num_test' : num_test, 
            'loss_train' : log_last_entry.get("loss_train"), 
            'loss_val'   : log_last_entry.get("loss_val"), 
            'loss_test'  : loss_test}]

utils.log(summary, "logs/summary.csv")


# In[ ]:


# Load the model

# import torch
# from modules import losses

# model = ADLinkNet.ADLinkNet50()
# model.load_state_dict(torch.load("/Users/ricardobatista/Box/Road detection (Summer 2020)/models/PyTorch template/weights/model_state_dict.pt"))
# model.eval()

# criterion = losses.DiceLoss()


# In[ ]:


# Prediction

import random
import modules.evaluate as evaluate

SIZE_PRED = set_mod.SIZE_PRED
size_pred = min(SIZE_PRED, num_test - 1)

paths_pred_im = random.sample(paths_im["test"], size_pred)

for path_pred_im in paths_pred_im:
    evaluate.get_predicted(path_pred_im, model, criterion)

