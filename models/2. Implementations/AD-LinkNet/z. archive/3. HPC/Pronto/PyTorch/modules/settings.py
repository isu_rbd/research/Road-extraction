# Dataset
PATH_DATASET = "../dataset_small"
data_split = {"train" : 0.64, "val" : 0.16, "test" : 0.2}


# Model-specific
IM_LEN = 608


# Training
NUM_EPOCHS = 30
BATCH_SIZE = 8


# Prediction
SIZE_PRED = 10


# The remaining settings are in main.py:
#  - Set the model
#  - Set the loss (default = DiceLoss)
#  - Set the optimizer (default = Adam)
