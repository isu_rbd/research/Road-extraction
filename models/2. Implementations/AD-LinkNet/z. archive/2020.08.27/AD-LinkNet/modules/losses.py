import torch
import numpy as np
from torch import nn
from torch import einsum


# https://github.com/rogertrullo/pytorch/blob/rogertrullo-dice_loss/torch/nn/functional.py#L708
class DiceLoss(nn.Module):
    def __init__(self):
        super(DiceLoss, self).__init__()

    def forward(self, input, target):
        """
        input is a torch variable of size BatchxnclassesxHxW representing log probabilities for each class
        target is a 1-hot representation of the groundtruth, shoud have same size as the input
        """
        print(input.shape)
        print(input)
        print(target.shape)
        print(target)
        
        assert input.size() == target.size(), "Input sizes must be equal."
        assert input.dim() == 4, "Input must be a 4D Tensor."
        uniques=np.unique(target.numpy())
        assert set(list(uniques))<=set([0,1]), "target must only contain zeros and ones"

        probs = input  # RB edit
        # probs=F.softmax(input) # RB edit
        num=probs*target#b,c,h,w--p*g
        num=torch.sum(num,dim=3)#b,c,h
        num=torch.sum(num,dim=2)


        den1=probs*probs#--p^2
        den1=torch.sum(den1,dim=3)#b,c,h
        den1=torch.sum(den1,dim=2)


        den2=target*target#--g^2
        den2=torch.sum(den2,dim=3)#b,c,h
        den2=torch.sum(den2,dim=2)#b,c


        dice=2*(num/(den1+den2))
        dice_eso=dice[:,1:]#we ignore bg dice val, and take the fg

        dice_total=-1*torch.sum(dice_eso)/dice_eso.size(0)#divide by batch_sz

        print(-1*torch.sum(dice_eso))
        
        return dice_total


# class DiceLoss(nn.Module):
#     def __init__(self, weight = None, size_average = True):
#         super(DiceLoss, self).__init__()

#     def forward(self, inputs, targets, smooth = 1):
        
        # comment out if your model contains a sigmoid or equivalent activation layer
        # inputs = F.sigmoid(inputs)       
        
#         #flatten label and prediction tensors
#         inputs = inputs.view(-1)
#         targets = targets.view(-1)
        
#         intersection = (inputs * targets).sum()                            
#         dice = (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)  
        
#         return 1 - dice



"""
Dice: cobbled together from sources below
Copy from: https://github.com/LIVIAETS/surface-loss/blob/108bd9892adca476e6cdf424124bc6268707498e/losses.py#L29
paper: https://arxiv.org/pdf/1707.03237.pdf
tf code: https://github.com/NifTK/NiftyNet/blob/dev/niftynet/layer/loss_segmentation.py#L279
"""
# class DiceLoss(nn.Module):
#     def __init__(self, smooth=1e-5):
# 
#         super(DiceLoss, self).__init__()
# 
#         self.smooth = smooth
# 
#     def forward(self, pred, gt):
#         
#         intersection: Tensor = einsum("bcwh,bcwh->bc", pred, gt)             # (batch size, num of classes, w, h)
#         union: Tensor = (einsum("bcwh->bc", pred) + einsum("bcwh->bc", gt))
#         divided: Tensor = 1 - (2 * intersection + 1e-10) / (union + 1e-10)
# 
#         loss = divided.mean()
        
#         return loss

    
"""
Dice: cobbled together from sources above and
https://medium.com/analytics-vidhya/pytorch-implementation-of-semantic-segmentation-for-single-class-from-scratch-81f96643c98c
"""
# class DiceLossVec(nn.Module):
#     def __init__(self, smooth=1e-5):
# 
#         super(DiceLossVec, self).__init__()
# 
#         self.smooth = smooth
# 
#     def forward(self, pred, gt):
#         
#         intersection: Tensor = einsum("bcwh,bcwh->bc", pred, gt)             # (batch size,c,w,h)
#         union: Tensor = (einsum("bcwh->bc", pred) + einsum("bcwh->bc", gt))
# 
#         divided: Tensor = 1 - (2 * intersection + 1e-10) / (union + 1e-10)
# 
#         loss = divided
#         
#         return loss
