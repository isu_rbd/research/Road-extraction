import sys
import os
import os.path
from os import path
import csv
import torch
import numpy as np

sys.path.append(r'/work/LAS/zhuz-lab/road_extraction/AD-LinkNet')
# sys.path.append(r'/Users/ricardobatista/Box/Road extraction/models/2. Implementations/AD-LinkNet/AD-LinkNet')

import modules.settings as set_mod


# Save checkpoint if a new best is achieved
def save_checkpoint(state, is_best, filename = set_mod.PATH_CHECKPOINT):

    np.save(set_mod.PATH_EPOCH, state['epoch'])
    
    if is_best:
        print ("=> Saving a new best", flush = True)
        torch.save(state, filename)  # save checkpoint
    else:
        print("Min loss: ", state["min_loss"], flush = True)
        print ("=> Validation Accuracy did not improve", flush = True)

        
def log(data_list, filename):
    
    csv_columns = sorted(data_list[0].keys())    
    
    if path.exists(filename):
        try:
            with open(filename, 'a') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames = csv_columns)
                for data in data_list:
                    writer.writerow(data)
        except IOError:
            print("I/O error", flush = True)
        
    else:
        try:
            with open(filename, 'w') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames = csv_columns)
                writer.writeheader()
                for data in data_list:
                    writer.writerow(data)
        except IOError:
            print("I/O error", flush = True)
