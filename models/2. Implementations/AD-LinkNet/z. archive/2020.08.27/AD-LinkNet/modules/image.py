# Image module

import os
import cv2
import numpy as np
import random
import glob
import torch
import torch.utils.data as Data

import modules.settings as set_mod

IM_LEN    = set_mod.IM_LEN

    
def get_paths_im(path, data_split):
    
    train, val, test = data_split["train"], data_split["val"], data_split["test"]
    
    paths_im = {"train" : [], "val" : [], "test" : []}
    
    paths_state = [f.path for f in os.scandir(path) if f.is_dir() if not f.name.startswith('.')]
    
    for path_state in paths_state:
        
        paths_state_im = glob.glob(path_state + "/im/*.png")
        random.shuffle(paths_state_im)
        num_im   = len(paths_state_im)
        breaks   = [int(num_im*train), int(num_im*(train + val))]
        train_temp, val_temp, test_temp = np.split(paths_state_im, breaks)
        
        paths_im["train"].extend(train_temp.tolist())
        paths_im["val"].extend(val_temp.tolist())
        paths_im["test"].extend(test_temp.tolist())
    
    return paths_im
    

def trans_im(path):
    im = cv2.imread(path).astype(np.float)/255.
    im = cv2.resize(im, (IM_LEN, IM_LEN), interpolation = cv2.INTER_AREA)
    x = np.zeros([1, 3, IM_LEN, IM_LEN])
    x[0, 0, :, :] = im[:, :, 0]
    x[0, 1, :, :] = im[:, :, 1]
    x[0, 2, :, :] = im[:, :, 2]
    return x


def trans_gt(path):
    gt = np.round_(cv2.imread(path).astype(np.float)/255.)
    gt = cv2.resize(gt, (IM_LEN, IM_LEN), interpolation = cv2.INTER_AREA)
    y = np.zeros([1, 1, IM_LEN, IM_LEN])
    y[0, 0, :, :] = 1 - gt[:, :, 1] 
    return y


def get_loader(paths_im, batch_size):
    image_num = len(paths_im)
    x = np.zeros([image_num, 3, IM_LEN, IM_LEN])
    y = np.zeros([image_num, 1, IM_LEN, IM_LEN])
    for i in range(image_num):
        path_im = paths_im[i]
        path_gt = path_im.replace("/im/", "/gt/")
        x[i, :, :, :] = trans_im(path_im)
        y[i, :, :, :] = trans_gt(path_gt)
    x = torch.FloatTensor(x)
    y = torch.FloatTensor(y)
    torch_dataset = Data.TensorDataset(x, y)
    loader = Data.DataLoader(
        dataset = torch_dataset,
        batch_size = batch_size,
        shuffle = True,
        num_workers = 4*2,
    )
    return loader


"""
import numpy as np
import cv2

path_gt = "/Users/ricardobatista/Box/Road extraction/Dataset/dataset/AZ/gt/AZ_0017.png"
IM_LEN = 608

gt = np.array(cv2.imread(path_gt, flags=0))
gt = cv2.resize(gt, (IM_LEN, IM_LEN), interpolation = cv2.INTER_AREA)
(thresh, blackAndWhiteImage) = cv2.threshold(gt, 127, 255, cv2.THRESH_BINARY) # 127 is middle of grayscale

mask = np.where(blackAndWhiteImage==0, 1, blackAndWhiteImage)
mask = np.where(mask==255, 0, mask)

y = np.zeros([1, 1, IM_LEN, IM_LEN])
y[0, 0, :, :] = mask
"""