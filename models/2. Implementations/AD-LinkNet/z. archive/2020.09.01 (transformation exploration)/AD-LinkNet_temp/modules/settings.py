#### General ####

# PATH_ROOT = "/work/LAS/zhuz-lab/road_extraction/"
PATH_ROOT = "/Users/ricardobatista/Box/Road extraction/"

PATH_IMPLEMENTATION = PATH_ROOT + "AD-LinkNet/"
# PATH_IMPLEMENTATION = PATH_ROOT + "models/2. Implementations/AD-LinkNet/AD-LinkNet/"
PATH_IMPLEMENTATION = PATH_ROOT + "models/2. Implementations/AD-LinkNet/AD-LinkNet_temp/"

IM_LEN = 608



#### Data ####

# PATH_DATASET = PATH_ROOT + "Dataset/dataset"
# PATH_DATASET = PATH_ROOT + "dataset_tiny"
PATH_DATASET = PATH_ROOT + "z. backend/5. Other datasets/dataset_tiny/dataset_tiny"

DATA_SPLIT = {"train" : 0.64, "val" : 0.16, "test" : 0.2}
BATCH_SIZE = 2 #8

PATH_LOADER_TRAIN = PATH_IMPLEMENTATION + "01_data/dataloaders/dataloader_train.pth"
PATH_LOADER_VAL   = PATH_IMPLEMENTATION + "01_data/dataloaders/dataloader_val.pth"
PATH_LOADER_TEST  = PATH_IMPLEMENTATION + "01_data/dataloaders/dataloader_test.pth"

PATH_TEST_IM = PATH_IMPLEMENTATION + "04_predict/test_cases.pickle"



#### Train ####

PATH_EPOCH      = PATH_IMPLEMENTATION + "02_train/checkpoints/epoch.npy"
PATH_CHECKPOINT = PATH_IMPLEMENTATION + "02_train/checkpoints/checkpoint.pth.tar"
NUM_EPOCHS = 4

PATH_LOG        = PATH_IMPLEMENTATION + "02_train/logs/logs.csv"


#### Prediction ####

SIZE_PRED = 40

# The remaining settings are in the .ipynb files:
#  - Set the model
#  - Set the loss (default = DiceLoss)
#  - Set the optimizer (default = Adam)

