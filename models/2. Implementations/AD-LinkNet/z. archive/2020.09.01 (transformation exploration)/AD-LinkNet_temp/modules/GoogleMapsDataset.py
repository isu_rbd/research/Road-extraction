import torch
import numpy as np
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms, utils
import random

import modules.settings as set_mod

IM_LEN = set_mod.IM_LEN


class GoogleMapsDataset(Dataset):
    
    def __init__(self, paths_image, norm = True, mean = None, std = None, augment = False):
        
        self.paths_image = paths_image
        self.norm = norm
        self.mean = mean
        self.std = std
        self.augment = augment
        
        
        # Transformations
        self.resize = transforms.Resize(IM_LEN, interpolation = Image.BICUBIC) # Image.BICUBIC is PIL default
        self.jitter = transforms.ColorJitter(brightness = 0.4, contrast = 0.4, saturation = 0.4, hue = 0.4)
        
        if norm:
            self.normalize = transforms.Normalize(self.mean, self.std)
            
        self.tfms = transforms.Compose([
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.RandomAffine(degrees = 30, scale = (.9, 1.1), translate = (0.25, 0.25)),
            transforms.ToTensor()
        ])
        
    def __len__(self):
        return len(self.paths_image)
        
        
    def __getitem__(self, idx):
        
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        # Get PIL images
        path_image = self.paths_image[idx]
        image = Image.open(path_image)
        mask  = Image.open(path_image.replace("/im/", "/gt/"))
        
        
        # Resize (so image and mask are a multiple of 32 and can thus work with AD-LinkNet code)
        image, mask = self.resize([image, mask])
        
        
        # Preliminary transformations
        ## image
        if self.augment:
            image = self.jitter(image)
        
        ## mask
        mask = mask.convert('L')                                    # grayscale
        mask = mask.point(lambda x: 0 if x < 128 else 255, '1')     # black and white
        
        
        # Image augmentation
        if self.augment:
            im, gt = self.tfms([im, gt])
        
        
        # Final transformations
        ## image
        image = np.einsum('hwc->chw', image)   # Change from (H, W, C) to (C, H, W) 
        if self.norm:
            image = self.normalize(image)
        
        ## mask
        mask = torch.where(mask == 255, 0, mask)     # mask is now a matrix of zeros (background) and ones (road)
        
        
        return image, mask

    
class Resize(object):
    
    def __init__(self, output_size = IM_LEN):
        self.output_size = output_size
        
    def __call__(self, sample):
        image, mask = sample['image'], sample['mask']
        
        image = image.resize((self.output_size, self.output_size)) # Image.BICUBIC is PIL default
        mask  = mask.resize((self.output_size, self.output_size))
        
        return {'image': image, 'mask': mask}

    
class HorizontalFlip(object):
    
    def __call_(self, sample):
        image, mask = sample['image'], sample['mask']
        
        prob = random.randint(0, 1)                        # i.e., get either 0 or 1 with equal probability
        tsfm = transforms.RandomHorizontalFlip(p = prob)   # flip with probability 0 or 1
        
        return {'image': tsfm(image), 'mask': tsfm(mask)}

    
class VerticalFlip(object):
    
    def __call_(self, sample):
        image, mask = sample['image'], sample['mask']
        
        prob = random.randint(0, 1)                      # i.e., get either 0 or 1 with equal probability
        tsfm = transforms.RandomVerticalFlip(p = prob)   # flip with probability 0 or 1
        
        return {'image': tsfm(image), 'mask': tsfm(mask)}

    
class Affine(object):
    
    def __init__(self, degrees = 30, scale = 0.1, translate = 0.25):
        self.degrees = degrees
        self.scale = scale
        self.translate = translate
        
    def __call__ (self, sample):
        image, mask = sample['image'], sample['mask']
        
        degrees = random.uniform(-self.degrees, self.degrees)
        trans_h = random.uniform(-IM_LEN*self.translate, IM_LEN*self.translate)
        trans_v = random.uniform(-IM_LEN*self.translate, IM_LEN*self.translate)
        scale   = random.uniform(1 - self.scale, 1 + self.scale)
        
        image = transforms.functional.affine(image, angle = degrees, translate = (trans_h, trans_v), scale = scale)
        mask  = transforms.functional.affine(mask, angle = degrees, translate = (trans_h, trans_v), scale = scale)
        
        return {'image': tsfm(image), 'mask': tsfm(mask)}
    
    
"""
# Maybe include a method that returns the file name
    
# https://pytorch.org/tutorials/recipes/recipes/custom_dataset_transforms_loader.html
# https://pytorch.org/tutorials/beginner/data_loading_tutorial.html

# Normalization
## https://stackoverflow.com/questions/49444262/normalize-data-before-or-after-split-of-training-and-testing-data#:~:text=Feature%20normalization%20(or%20data%20standardization,and%20dividing%20by%20the%20variance.&text=In%20this%20way%2C%20we%20can,to%20new%2C%20unseen%20data%20points.

# Normalize after other transforms
## https://discuss.pytorch.org/t/color-augmentation-before-of-after-normalisation-for-resnet/22537

# Bicubic
https://codesuche.com/python-examples/PIL.Image.BICUBIC/?ipage=3
"""
