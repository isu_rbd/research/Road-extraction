## Table of contents

0. To-do
1. Preprocessing
2. Main
3. Other considerations


## 0. To-do

- Figure out if we need to specify the "device"
- Post-processing
- Data augmentation
- Figure out how to input 600x600 image




## 1. Preprocessing


### Loading images

- Custom loader for image segmentation.
- In "trans_gt":
    - We use either green (i.e., 1) or red (i.e., 2) channel to identify ON/OFF pixels.
    - Then, we set OFF = 0 and ON = 1.
- Number num_worker = 4 * num_GPU
- "When you are training, the batch size is an important hyper-parameter which has an affect on the properties and final results of the training process. During test/validation it has no affect and only needs to be small enough for your model to be able to run it (evaluation with different batch sizes will produce the same results)."
    - RB: Based on this, I set the size of validation set to 8 (like the training data set) since 

Sources: 
- https://github.com/vc-nju/RAS_python
- https://www.programcreek.com/python/example/100894/torch.utils.data.TensorDataset
- https://discuss.pytorch.org/t/guidelines-for-assigning-num-workers-to-dataloader/813/3
- https://ai.stackexchange.com/questions/10281/testing-validation-percentage-test-validation-batch-size-difference


### Dataset split

- Split your data into training and testing (80/20 is indeed a good starting point)
- Split the training data into training and validation (again, 80/20 is a fair split).

Sources:
- https://stackoverflow.com/questions/13610074/is-there-a-rule-of-thumb-for-how-to-divide-a-dataset-into-training-and-validatio#:~:text=Split%20your%20data%20into%20training,performance%20on%20the%20validation%20set






### Data augmentation

We might be able to implement the augmentation we need using torchvision.transforms.Compose(transforms).

Sources:
- https://pytorch.org/docs/stable/torchvision/transforms.html




## 2. Training

### General algorithm

- Structure based on 
    * source from towardsdatascience (see source)
    * Stanford's 2018 CNN course "Lecture 8 | Deep Learning Software."
- Modifications made to inner loop:
    - assuming aforementioned code mistakenly does not call on x_batch nor y_batch. Modified code per RAS source below.
    - got rid of "x_var, y_var = Variable(x_batch), Variable(y_batch)" since `Variables` is deprecated (see sources).

Source:
- https://towardsdatascience.com/understanding-pytorch-with-an-example-a-step-by-step-tutorial-81fc5f8c4e8e
- https://www.youtube.com/watch?v=6SlgtELqOWc&list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv&index=9&t=0s
- https://github.com/vc-nju/RAS_python/blob/master/train.py
- https://discuss.pytorch.org/t/runtimeerror-only-batches-of-spatial-targets-supported-3d-tensors-but-got-targets-of-dimension-4/82098/2


### Batch size

"Batchsize is set between 2 and 8 according to the network" (AD-LinkNet)

Sources: 
- https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8700168



### Loss functions



#### Dice loss


https://github.com/LIVIAETS/surface-loss/blob/108bd9892adca476e6cdf424124bc6268707498e/losses.py#L29

Implemented function at first link with the following modifications:
- Got rid of "c" in all einsums
- Got rid of references to "softmax_output"

Sources:
- https://github.com/JunMa11/SegLoss/blob/master/losses_pytorch/dice_loss.py
- https://www.kaggle.com/c/ultrasound-nerve-segmentation/discussion/21358


#### Dice loss vectorized form (for val and testing)

This way, the dice loss is the same regardless of batch size.

Sources:
- https://medium.com/analytics-vidhya/pytorch-implementation-of-semantic-segmentation-for-single-class-from-scratch-81f96643c98c



#### "Global" Dice loss (i.e., dice loss calculated by concatenating all images in entire batch)

Implementation from Kaggle source (below).

Source:
- https://www.kaggle.com/bigironsphere/loss-function-library-keras-pytorch


#### IoU loss

Implementation available at same source.

Source:
- https://www.kaggle.com/bigironsphere/loss-function-library-keras-pytorch


#### Cross entropy loss

Not sure why this isn't working:

    # Setup
    criterion = torch.nn.CrossEntropyLoss()
    
    ...
    
    # Inside loop
    loss = criterion(y_pred, torch.flatten(y_batch, start_dim = 0, end_dim = 1).long())

    # Error message
    IndexError: Target 1 is out of bounds.

I chose CrossEntropyLoss since "The main reason that people try to use dice coefficient or IoU directly is that the actual goal is maximization of those metrics, and cross-entropy is just a proxy which is easier to maximize using backpropagation."

"For the choice of loss function, the road occupy a small proportion of the overall picture, but the background have a large percentage in the road extraction dataset. For the land classification dataset, the proportions of the segmentation target and the background are also unbalanced. So we choose to use Dice loss instead of IoU loss." (AD-LinkNet)

Pay close attention to the form of the "target" parameter in CrossEntropyLoss. This is why we flatten the tensor.

- Cast target tensor to Long cause that's what CrossEntropyLoss takes (i.e., "RuntimeError: expected scalar type Long but found Float")



Sources:
- https://pytorch.org/docs/stable/nn.html
- https://stats.stackexchange.com/questions/321460/dice-coefficient-loss-function-vs-cross-entropy#:~:text=In%20general%2C%20it%20seems%20likely,easier%20to%20maximize%20using%20backpropagation.
- https://pytorch.org/docs/master/generated/torch.nn.CrossEntropyLoss.html



### Optimizer

"And using Adam and RMSProp as the optimizer, the initial learning rate is 1e-4, and the network loss function tends to be stable."

Sources:
- (AD-LinkNet) https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8700168



### Validation workflow

Structure drawn from source below.

Sources:
- https://towardsdatascience.com/understanding-pytorch-with-an-example-a-step-by-step-tutorial-81fc5f8c4e8e



### Device

A torch.device is an object representing the device on which a torch.Tensor is or will be allocated. The torch.device contains a device type ('cpu' or 'cuda') and optional device ordinal for the device type.

See 2nd source for implementation.

How to use multiple GPUs (DataParallel) for training a model? You can just wrap your model in DataParallel and specify the device_ids you would like to use. The data will be split among the batch dimensions. That is, nn.DataParallel(model).


Sources:
- https://pytorch.org/docs/stable/tensor_attributes.html#torch-device
- https://pytorch.org/docs/stable/notes/cuda.html
- https://towardsdatascience.com/understanding-pytorch-with-an-example-a-step-by-step-tutorial-81fc5f8c4e8e
- https://pytorch.org/tutorials/beginner/blitz/data_parallel_tutorial.html
- https://discuss.pytorch.org/t/how-to-use-multiple-gpus-dataparallel-for-training-a-model-that-used-to-use-one-gpu/20150/2


### Checkpoints

Sources:
- https://blog.floydhub.com/checkpointing-tutorial-for-tensorflow-keras-and-pytorch/



### Saving dataloaders

Sources:
- https://discuss.pytorch.org/t/how-to-save-dataloader/62813/3



## 3. Other considerations


### Class imbalance

"However, class imbalance is typically taken care of simply by assigning loss multipliers to each class, such that the network is highly disincentivized to simply ignore a class which appears infrequently, so it's unclear that Dice coefficient is really necessary in these cases."

We might be able to address class imbalance by modifying the appropriate "weight" parameter in CrossEntropyLoss(weight=None).


Sources:
- https://pytorch.org/docs/stable/nn.html
- https://stats.stackexchange.com/questions/321460/dice-coefficient-loss-function-vs-cross-entropy#:~:text=In%20general%2C%20it%20seems%20likely,easier%20to%20maximize%20using%20backpropagation.


### Singularity

How to use cluster GPUs.

Sources:
- https://www.hpc.iastate.edu/guides/containers
