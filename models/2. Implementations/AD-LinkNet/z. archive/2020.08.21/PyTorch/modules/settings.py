# Dataset
PATH_DATASET = "/Users/ricardobatista/Box/Road detection (Summer 2020)/z. backend/5. Other datasets/dataset_small/dataset"
data_split = {"train" : 0.64, "val" : 0.16, "test" : 0.2}


# Model-specific
IM_LEN = 608


# Training
NUM_EPOCHS = 20
BATCH_SIZE = 8


# Prediction
SIZE_PRED = 10


# The remaining settings are in main.py:
#  - Set the model
#  - Set the loss (default = DiceLoss)
#  - Set the optimizer (default = Adam)
