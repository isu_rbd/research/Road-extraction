######################################################################
##
## Implementation of GL-Dense-U-Net proposed by Xu et al. 2018, MDPI
##
## Reference Code: https://github.com/cugxyy/GL-Dense-U-Net
##
## Modification to be written in Keras by Minsung Jang
##
##
######################################################################

from keras import backend as K
from keras.models import Model
from keras.layers import Input, concatenate, multiply, Activation, BatchNormalization, Reshape
from keras.layers import Conv2D, MaxPooling2D, Conv2DTranspose, AveragePooling2D
from keras.optimizers import Adam
from keras.losses import binary_crossentropy

from model.losses import bce_dice_loss, dice_loss, dice_coeff

layers_per_block = [4, 5, 7, 10, 12]
nb_blocks = len(layers_per_block)
growth_rate = 16

def conv_block(x, training, growth_rate, name):
    
    ## Composition function of Batch Normalization and ReLU Activation
    y = BatchNormalization(axis=3, momentum=0.997, epsilon=1e-5, scale=True, trainable=training)(x)
    y = Activation('relu', name=name + '_0_relu')(y)
    
    ## Conv 3 x 3
    conv = Conv2D(growth_rate, (3, 3), strides=(1, 1), padding='same', activation='relu', name=name+'_conv3x3')(y)
    
    return conv

def dense_block(x, training, block_nb, growth_rate, name):
    
    dense_out = []
        
    for i in range(layers_per_block[block_nb]):
        conv = conv_block(x, training, growth_rate, name + '_block' + str(i+1))
        x = concatenate([conv, x], axis=3)
        dense_out.append(conv)
    
    concat = concatenate(dense_out, axis=3)
        
    return concat

def trans_down(x, training, name):
    
    bn_axis = 3
    channel = int(K.int_shape(x)[bn_axis])
    
    y = Conv2D(channel, (1, 1), strides=(1, 1), padding='same', activation='relu', name=name+'_conv1x1')(x)
    y = MaxPooling2D(pool_size=(4, 4), strides=(2, 2), padding='same', name=name+'_maxpool2x2')(y)
    
    return y

def trans_up(x, name):
    
    bn_axis = 3
    channel = int(K.int_shape(x)[bn_axis])
    
    x = Conv2DTranspose(channel, (3, 3), strides=(2, 2), padding='same', activation='relu', name=name+'_upconv3x3')(x)
    
    return x

def global_attention_unit(x, name):
    
    bn_axis=3
    channel = int(K.int_shape(x)[bn_axis])
    k_size = int(K.int_shape(x)[2])
    
    gap = AveragePooling2D(pool_size=(k_size, k_size), strides=(1, 1), padding='same',name=name+'_global_pool')(x)
    y = Conv2D(channel, (1, 1), strides=(1, 1), padding='same', activation='relu', name=name+'_global_conv1x1')(gap)
    y = Conv2DTranspose(channel, (3, 3), strides=(2, 2), padding='same', activation='relu', name=name+'_global_poolup')(y) 
    
    return y

def local_attention_unit(x, name):
    
    bn_axis=3
    channel = int(K.int_shape(x)[bn_axis])
    
    conv_1x1 = Conv2D(channel, (1, 1), strides=(1, 1), padding='same', activation='relu', name=name+'_local_conv1x1')(x)
    conv_3x3 = Conv2D(channel, (3, 3), strides=(8, 8), padding='same', activation='relu', name=name+'_local_conv3x3')(x)
    conv_5x5 = Conv2D(channel, (5, 5), strides=(4, 4), padding='same', activation='relu', name=name+'_local_conv5x5')(x)
    conv_7x7 = Conv2D(channel, (7, 7), strides=(2, 2), padding='same', activation='relu', name=name+'_local_conv7x7')(x)
    
    conv_3x3_up = Conv2DTranspose(channel, (3, 3), strides=(2, 2), padding='same', activation='relu',
                                  name=name+'_local_up_conv3x3')(conv_3x3)    
    conv_3x5 = concatenate([conv_3x3_up, conv_5x5], axis=3, name=name+'_concat_3x5')
    conv_3x5_up = Conv2DTranspose(channel, (3, 3), strides=(2, 2), padding='same', activation='relu',
                                  name=name+'_local_up_conv3x5')(conv_3x5)
    conv_5x7 = concatenate([conv_3x5_up, conv_7x7], axis=3, name=name+'_concat_5x7')
    conv_5x7_up = Conv2DTranspose(channel, (3, 3), strides=(2, 2), padding='same', activation='relu',
                                  name=name+'_local_up_conv5x7')(conv_5x7)
    
    prod = multiply([conv_1x1, conv_5x7_up])
    
    res = Conv2D(channel, (1, 1), strides=(1, 1), padding='same', activation='relu', name=name+'_lau')(prod)
    
    return res

def get_gl_dense_unet(input_shape=(256,256,3), num_classes=2):
        
    channel0 = 48
    training = True
    
    ## Input Tensor
    inputs = Input(shape=input_shape)
    
    ##### Encoding Part #####
    concats = []
    
    ## [1] Conv 3 x 3 : 256 x 256 x 3  
    x = Conv2D(channel0, (3, 3), strides=(1, 1), padding='same', activation='relu', name='first_conv3x3')(inputs)
    
    ## [2] DenseNet - 1,2,3,4,5
    print('Start Downsampling!!!')
    for block_nb in range(nb_blocks):
        
        dense = dense_block(x, training, block_nb, growth_rate, 'dense_block_down_'+str(block_nb+1))
        x = concatenate([x, dense], axis=3, name='concat_down_'+str(block_nb+1))
        
        concats.append(x)
        
        if block_nb < nb_blocks-1:
            x = trans_down(x, training, name='trans_down_'+str(block_nb+1))
            
        print(K.int_shape(x))
        
    ## [3] LAU at the last layer: 16 x 16 x 656
    x_last = local_attention_unit(x, 'last_layer')
    print('Last Layer after LAU: ', K.int_shape(x_last))
    
    ##### Decoding Part #####
    
    ## [4] GAU and LAU
    print('Start Upsampling!!!')
    for i in range(nb_blocks-1):
        
        bn_axis = 3
        channel = int(K.int_shape(x_last)[bn_axis])
        block_nb = nb_blocks-1 - i
        
        ## Global Attention Unit (GAU)
        x = trans_up(x_last, 'trans_up_'+str(block_nb))
        gau = global_attention_unit(x_last, 'gau_'+str(block_nb))
        x = concatenate([x, gau], axis=3, name='concat_up_gau_'+str(block_nb))
        
        ## Local Attention Unit (LAU)
        lau = concats[len(concats) - i - 2]
        
        if (i!=1):
            lau = local_attention_unit(lau, 'lau_'+str(block_nb))
        
        ## LAU + GAU
        x = concatenate([x, lau], axis=3, name='concat_up'+str(block_nb))
        
        ## Conv 1x1 with LAU + GAU
        x = Conv2D(channel, (1, 1), strides=(1, 1), padding='same', activation=None, name='concat_up1x1'+str(block_nb))(x)
        
        ## Dense Block: Upscale
        x = dense_block(x, training, block_nb, growth_rate, 'dense_block_up_'+str(block_nb))
        
        ## Switch the input layer to the next layer
        x_last = x
        
        print(K.int_shape(x))
    
    ### [5] Conv 1x1 at the last
    classifier = Conv2D(num_classes, (1, 1), strides=(1, 1), padding='same', name='last_conv1x1', activation='softmax')(x)
    print('ouput layer :', K.int_shape(classifier))
    
    model = Model(inputs=inputs, outputs=classifier)

    model.compile(optimizer=Adam(), loss=bce_dice_loss, metrics=[dice_coeff])

    return model