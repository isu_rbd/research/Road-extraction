# Modifications to multi_gpu_train.py

## Uprade to TF 2.0

### Automatic updates

Used script to automatically upgrade code to TensorFlow 2:

`
tf_upgrade_v2 \
  --intree GL-Dense-U-Net/ \
  --outtree GL-Dense-U-Net_v2/ \
  --reportfile report.txt
`

Sources:
- https://www.tensorflow.org/guide/upgrade

### Manual updates

Using member tf.contrib.slim in deprecated module tf.contrib. tf.contrib.slim cannot be converted automatically. tf.contrib will not be distributed with TensorFlow 2.0, please consider an alternative in non-contrib TensorFlow, a community-maintained repository such as tensorflow/addons, or fork the required code.

pip install --upgrade tf_slim


Then, I replaced `from tensorflow.contrib import slim` with `import tf_slim as slim` and 

# slim = tf_slim  # RB commented out


Sources:
- https://stackoverflow.com/questions/58628439/upgrading-tf-contrib-slim-manually-to-tf-2-0
- https://github.com/google-research/tf-slim


## Path changes

### Basic

sys.path.append(r'/media/cugxyy/c139cfbf-11c3-4275-9602-b96afc28d10c/DL/Road_Segmentation')
sys.path.append(r'/Users/ricardobatista/Box/Road detection (Summer 2020)/models/GL-Dense-U-Net/Road_Segmentation')


tf.compat.v1.app.flags.DEFINE_string('checkpoint_path', r'/media/cugxyy/c139cfbf-11c3-4275-9602-b96afc28d10c/DL/Road_Segmentation/checkpoints_fcn/', '')
tf.compat.v1.app.flags.DEFINE_string('checkpoint_path', r'/Users/ricardobatista/Box/Road detection (Summer 2020)/models/GL-Dense-U-Net/Road_Segmentation/checkpoints_fcn/', '')


tf.compat.v1.app.flags.DEFINE_string('logs_path', r'/media/cugxyy/c139cfbf-11c3-4275-9602-b96afc28d10c/DL/Road_Segmentation/logs/', '')
tf.compat.v1.app.flags.DEFINE_string('logs_path', r'/Users/ricardobatista/Box/Road detection (Summer 2020)/models/GL-Dense-U-Net/Road_Segmentation/logs/', '')

tf.compat.v1.app.flags.DEFINE_string('training_data_path',
                           r'/media/cugxyy/c139cfbf-11c3-4275-9602-b96afc28d10c/DL/Road_Segmentation/traintemp.tfrecords',
                           '')
tf.compat.v1.app.flags.DEFINE_string('training_data_path',
                           r'/Users/ricardobatista/Box/Road detection (Summer 2020)/models/GL-Dense-U-Net/Road_Segmentation/traintemp.tfrecords',
                           '')


### Settings

    filename_queue = tf.compat.v1.train.string_input_producer([FLAGS.training_data_path], num_epochs=1000)
    filename_queue = tf.compat.v1.train.string_input_producer([FLAGS.training_data_path], num_epochs=2)


tf.compat.v1.app.flags.DEFINE_integer('train_size', 256, '')
tf.compat.v1.app.flags.DEFINE_integer('train_size', 600, '')


# Modifications to tf_records.py


## Path changes

### Basic

target_dir = r'/home/xu/PycharmProjects/Road_Segmentation/Contour_detection_data/all_train_data/label'
target_dir = r'/Users/ricardobatista/Box/Road detection (Summer 2020)/z. backend/5. Other datasets/dataset_tiny/dataset_tiny/ME/gt'

img_dir = r'/home/xu/PycharmProjects/Road_Segmentation/Contour_detection_data/all_train_data/image'
img_dir = r'/Users/ricardobatista/Box/Road detection (Summer 2020)/z. backend/5. Other datasets/dataset_tiny/dataset_tiny/ME/im'


### Settings

Changed the image size to 600x600 from 256x256