# Modifications to multi_gpu_train.py

## Uprade to TF 2.0

### Automatic updates

Used script to automatically upgrade code to TensorFlow 2:

`
tf_upgrade_v2 \
  --intree GL-Dense-U-Net/ \
  --outtree GL-Dense-U-Net_v2/ \
  --reportfile report.txt
`

Sources:
- https://www.tensorflow.org/guide/upgrade

### Manual updates


#### tf.contrib.slim

Using member tf.contrib.slim in deprecated module tf.contrib. tf.contrib.slim cannot be converted automatically. tf.contrib will not be distributed with TensorFlow 2.0, please consider an alternative in non-contrib TensorFlow, a community-maintained repository such as tensorflow/addons, or fork the required code.

pip install --upgrade tf_slim


Then, I replaced `from tensorflow.contrib import slim` with `import tf_slim as slim` and 


#### TFRecordReader

- Original: TFRecordReader
    - https://www.tensorflow.org/api_docs/python/tf/compat/v1/TFRecordReader
- Immediate replacement: tf.compat.v1.io.tf_record_iterator
    - https://github.com/tensorflow/tensorboard/issues/1711
- Latest version (and one I implemented): iter(tf.data.TFRecordDataset(FILEPATH_WHATEVER))
    - https://www.tensorflow.org/api_docs/python/tf/data/TFRecordDataset
    - iter:
        - https://www.tensorflow.org/api_docs/python/tf/data/TFRecordDataset#__iter__
        - https://treyhunner.com/2018/06/how-to-make-an-iterator-in-python/
        - https://www.programiz.com/python-programming/iterator

#### string_input_producer

Maybe use https://stackoverflow.com/questions/54509752/how-to-translate-deprecated-tf-train-queuerunners-tensorflow-approach-to-importi.


#### shuffle_batch

##### First attempt

    #     resized_pairs = tf.data.Dataset([resized_image, resized_annotation])
    #     image_batch, annotation_batch = resized_pairs.shuffle(buffer_size = 500).batch(batch_size = FLAGS.batch_size * len(gpus))

##### Second attempt

    ## Create a random shuffle queue.
    queue = tf.queue.RandomShuffleQueue(capacity = 1000, min_after_dequeue = 500, 
                                        dtypes = [resized_image.dtype, resized_annotation.dtype])
    
    enqueue = queue.enqueue([resized_image, resized_annotation])
    
    num_threads = 4
    qr = tf.train.QueueRunner(queue, [enqueue] * num_threads)
    
    tf.train.add_queue_runner(qr)
    
    batch_size = FLAGS.batch_size * len(gpus)
    image_batch, annotation_batch = queue.dequeue_many(batch_size)


Sources:
- https://www.tensorflow.org/api_docs/python/tf/queue/RandomShuffleQueue
- https://tensorflow.juejin.im/api_guides/python/threading_and_queues.html
- https://www.programcreek.com/python/example/90374/tensorflow.RandomShuffleQueue


#### Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA

- Seems like solution is at source below but it's too damn complicated!

Sources:
- https://technofob.com/2019/06/14/how-to-compile-tensorflow-2-0-with-avx2-fma-instructions-on-mac/



# slim = tf_slim  # RB commented out


Sources:
- https://stackoverflow.com/questions/58628439/upgrading-tf-contrib-slim-manually-to-tf-2-0
- https://github.com/google-research/tf-slim


## Path changes

### Basic

sys.path.append(r'/media/cugxyy/c139cfbf-11c3-4275-9602-b96afc28d10c/DL/Road_Segmentation')
sys.path.append(r'/Users/ricardobatista/Box/Road detection (Summer 2020)/models/GL-Dense-U-Net/Road_Segmentation')


tf.compat.v1.app.flags.DEFINE_string('checkpoint_path', r'/media/cugxyy/c139cfbf-11c3-4275-9602-b96afc28d10c/DL/Road_Segmentation/checkpoints_fcn/', '')
tf.compat.v1.app.flags.DEFINE_string('checkpoint_path', r'/Users/ricardobatista/Box/Road detection (Summer 2020)/models/GL-Dense-U-Net/Road_Segmentation/checkpoints_fcn/', '')


tf.compat.v1.app.flags.DEFINE_string('logs_path', r'/media/cugxyy/c139cfbf-11c3-4275-9602-b96afc28d10c/DL/Road_Segmentation/logs/', '')
tf.compat.v1.app.flags.DEFINE_string('logs_path', r'/Users/ricardobatista/Box/Road detection (Summer 2020)/models/GL-Dense-U-Net/Road_Segmentation/logs/', '')

tf.compat.v1.app.flags.DEFINE_string('training_data_path',
                           r'/media/cugxyy/c139cfbf-11c3-4275-9602-b96afc28d10c/DL/Road_Segmentation/traintemp.tfrecords',
                           '')
tf.compat.v1.app.flags.DEFINE_string('training_data_path',
                           r'/Users/ricardobatista/Box/Road detection (Summer 2020)/models/GL-Dense-U-Net/Road_Segmentation/traintemp.tfrecords',
                           '')


### Settings

    filename_queue = tf.compat.v1.train.string_input_producer([FLAGS.training_data_path], num_epochs=1000)
    filename_queue = tf.compat.v1.train.string_input_producer([FLAGS.training_data_path], num_epochs=2)


tf.compat.v1.app.flags.DEFINE_integer('train_size', 256, '')
tf.compat.v1.app.flags.DEFINE_integer('train_size', 600, '')


# Modifications to tf_records.py


## Path changes

### Basic

target_dir = r'/home/xu/PycharmProjects/Road_Segmentation/Contour_detection_data/all_train_data/label'
target_dir = r'/Users/ricardobatista/Box/Road detection (Summer 2020)/z. backend/5. Other datasets/dataset_tiny/dataset_tiny/ME/gt'

img_dir = r'/home/xu/PycharmProjects/Road_Segmentation/Contour_detection_data/all_train_data/image'
img_dir = r'/Users/ricardobatista/Box/Road detection (Summer 2020)/z. backend/5. Other datasets/dataset_tiny/dataset_tiny/ME/im'


### Settings

Changed the image size to 600x600 from 256x256