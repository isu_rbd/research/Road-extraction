import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.metrics import confusion_matrix
from matplotlib.patches import Rectangle

import batch_module as bm

def getSuffixString(_sm, model_name):
    
    suffix = _sm.custom_suffix +'_'+_sm.strategy_name+'_'+model_name
    
    return suffix

def getDiceCoeff(y_true, y_pred, thresh=1.0) : ## avoid the case divisor == 0
    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()
    intersection = sum(y_true_f * y_pred_f) ## do AND operation and get the total sum
    score = 2.0 * ( intersection + thresh ) / ( sum(y_true_f) + sum(y_pred_f) + thresh )
    score = round(score, 3)
    
    return score

def getConfusionMatrix(y_true, y_pred) :
    
    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()
    tn, fp, fn, tp  = confusion_matrix(y_true_f, y_pred_f).ravel()
    
    ##     true / false    : match pred - actual
    ## postivie / negative : pred yes or no
    
    tpr = round (tp / ( tp + fn ), 3 ) ## among "actual yes" ( True Positive Rate, Sensitivity )
    fpr = round( fp / ( tn + fp ), 3 ) ## among "acutal no"  ( False Poistive Rate, Specificity )
    ppv = round( tp / ( tp + fp ), 3 ) ## among "pred yes"   ( Positive Predictive Value )
    npv = round( tn / ( tn + fn ), 3 ) ## among "pred no"    ( Negative Predictive Value )
 
    return tpr, fpr, ppv, npv

def saveTestStatistics(_im, suffix, model, window_size, doResize=True, verbose=True):
    
    total_df = _im.total_df
    strategy_name = _im.strategy_name
    custom_suffix = _im.custom_suffix
    
    test_df = total_df[total_df['class']=='T']
    df_index = test_df.index
    test_df=test_df.reset_index(drop=True)
    
    ## Save test result
    test_string = './summary/teststat_'+suffix
    teststat_csv = test_string+'.csv'
    teststat_txt = test_string+'.txt'
    
    ## Log file
    output_file = open(teststat_txt,"w")
    output_file.write("Window Size: "+str(window_size)+'\n')
    output_file.write("Input Size: "+str(_im.input_img_size)+'\n')
    output_file.write("Elapsed time: "+str(_im.training_time)+'[sec] \n')
    if (doResize):
        output_file.write("Resize \n")
    else:
        output_file.write("Keep \n")
    output_file.close()
    print("Save a logfile: ", teststat_txt)
    
    tmp_df = pd.DataFrame(columns=['dice','TPR','FPR','PPV','NPV'])
    
    for i_row in range(len(test_df)):
        
        nID = test_df['nID'].iloc[i_row]
        y_pred, y_true=_im.predictSingleImage(model, nID, window_size, doResize, verbose)
        
        dice_coeff = getDiceCoeff(y_true,y_pred)
        tpr, fpr, ppv, npv = getConfusionMatrix(y_true, y_pred)
        
        tmp_df=tmp_df.append({'dice': dice_coeff, 'TPR':tpr, 'FPR':fpr, 'PPV': ppv, 'NPV': npv},ignore_index=True)
       
    ## Concatenate dataframe
    stat_df = [ test_df, tmp_df]
    stat_df = pd.concat(stat_df, axis=1)
    stat_df.index=df_index
    stat_df.to_csv(teststat_csv, index=True, header=True)
    print("Save a dataframe: ", teststat_csv)
    
    return stat_df

def plotHistogram(stat_df, suffix):
    
    teststat = []
    stat_title = ['dice','TPR','FPR','PPV','NPV']
    plot_title = ['Dice Score','True Positive Rate (Sensitivity)', 'False Positive Rate (Specificity)',
                  'Postive Predictive Value ', 'Negative Predictive Value']
    clr_title = ['forestgreen', 'azure', 'tomato', 'navy','crimson']

    for i in range(len(stat_title)):
        title = stat_title[i]
        mean= round(np.mean(stat_df[title]),3)
        std= round(np.std(stat_df[title]),3)

        mean_str = "Mean : "+str(mean)
        std_str="Stddev: "+str(std)
        num_str="# images: "+str(len(stat_df))
        labels=[mean_str, std_str,num_str]
        handles = [Rectangle((0,0),0,0,color="white") for c in labels]

        bin_edges=list(np.linspace(0,1,11))
        if (std * 3 < 0.1 ):
            bin_edges = list(np.linspace(max(-0.2+round(mean,1),0), min(0.2+round(mean,1),1), 11))
        fig=plt.figure(i+1, figsize=[5,3], dpi=240)
        plt.hist(stat_df[stat_title[i]],
                 bins=bin_edges,
                 density=False,
                 histtype='bar',
                 color=clr_title[i],
                 edgecolor='black', alpha=0.7)

        plt.xlabel('Ratio')
        plt.xticks(bin_edges)
        plt.ylabel('Number of images')
        plt.title(plot_title[i])
        plt.legend(handles, labels,fontsize=12)
        fig.savefig("./plots/fig_"+stat_title[i]+"_"+suffix+".png", dpi=fig.dpi)
   
