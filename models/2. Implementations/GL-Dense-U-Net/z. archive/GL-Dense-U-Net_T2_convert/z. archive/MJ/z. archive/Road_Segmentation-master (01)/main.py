#!/usr/bin/env python
# coding: utf-8

# ## Road Segmentations in the images from Google Maps 

# ## [1] Load "setting.param" + * Check the sizes of the raw images in datasets

# In[1]:


import setting_module as set_mod
sm = set_mod.SettingModule()
checkImageSize = False
if (checkImageSize):
    sm.checkImageSize()


# ## [2] Sampling Strategy
# ## - input : `total_df` / - output: `sample_df`

# In[2]:


import strategy_module as strategy
sm.total_df = strategy.Resize(sm.total_df, sm.n_resize, sm.input_img_size)


# ## [3] Load the image module

# In[3]:


import image_module as img_mod
im = img_mod.ImageModule(sm)


# ## [4] Batch Grouping and Learning Step Decision: lists of batch information
# ## - input: `sample_df` /
# ## - return: `batch_train`, `batch_valid`, `n_train_steps`, `n_valid_steps`
# * **Define a batch**
# 
# ```
#   | (nID[image], crop_x, crop_y) 1          | 
#   | (nID[image], crop_x, crop_y) 2          |
#   |     ......                              |
#   | (nID[image], crop_x, crop_y) batch_size |
# ```
# 
# * **Generate `train` and `valid` batch lists respectively**

# In[4]:


import batch_module as bm
sample_limit = 200
bm.getBatchSize(sm.batch_size)
batch_train, batch_valid, n_train_steps, n_valid_steps = bm.groupBatch(sm.total_df, sample_limit)


# ## [5] Generate batch images from the batch lists
# ## - input: `batch_train`, `batch_valid`
# ## - return: `x_batch`, `y_batch` just one step before `Generator`

# In[5]:


def trainGenerator():
    while True:
        
        batch_list = batch_train
        
        for batch in batch_list:
            
            x_batch, y_batch = bm.generateBatchImages(im, batch, sm.total_df)
            
            yield x_batch, y_batch

def validGenerator():
    while True:
        
        batch_list = batch_valid
        
        for batch in batch_list:
            
            x_batch, y_batch = bm.generateBatchImages(im, batch, sm.total_df)

            yield x_batch, y_batch


# ## Model Training

# In[6]:


import time
import evaluate_module as em
from keras.callbacks import ReduceLROnPlateau, ModelCheckpoint, TensorBoard

def evalTotalModel(model_name, model, window_size, doResize=True, verbose=True):
    
    start_time = time.time()
    
    suffix = em.getSuffixString(sm, model_name)
    print("Saving suffix: ", suffix)
        
    weight_file_every_epoch = './weights/updateW_'+suffix+'.hdf5'
    weight_file_final ='./weights/bestW_'+suffix+'.h5'
    
    callbacks = [ReduceLROnPlateau(monitor='val_loss',
                           factor=sm.pl_factor,
                           patience=sm.pl_patience,
                           verbose=1,
                           min_delta=sm.pl_mindelta),
             ModelCheckpoint(monitor='val_loss',
                         filepath=weight_file_every_epoch,
                         save_best_only=True,
                         save_weights_only=True),
             TensorBoard(log_dir='logs')]

    model.fit_generator(generator=trainGenerator(),
                            steps_per_epoch=n_train_steps,
                            epochs=sm.epoch,
                            verbose=2,
                            callbacks=callbacks,
                            validation_data=validGenerator(),
                            validation_steps=n_valid_steps)

    model.save(weight_file_final)

    ## Check elapsed time
    end_time = time.time()
    im.training_time = end_time - start_time
    print("Elapsed Time: ", im.training_time, "[sec]")
    
    ## Make log file
    stat_df = em.saveTestStatistics(im, suffix, model, window_size, doResize, verbose)
    
    ## Plot histograms
    em.plotHistogram(stat_df, suffix)


# In[7]:


doResize=True
verbose=True
window_size = im.input_img_size

## Pure U-Net
from model.u_net import get_unet_256
model_unet = get_unet_256()
evalTotalModel("unet", model_unet, window_size, doResize, verbose)

# GL-Dense-U-Net
from model.gl_dense_u_net import get_gl_dense_unet
model_gl = get_gl_dense_unet()
evalTotalModel("gl", model_gl, window_size, doResize, verbose)


# ## Re-plotting histrograms using "summary csv" file

# In[8]:


# import pandas as pd

# suffix="chi37_resize_gl"
# stat_df = pd.read_csv("./summary/teststat_chi37_resize_gl.csv")
# em.plotHistogram(stat_df, suffix)

