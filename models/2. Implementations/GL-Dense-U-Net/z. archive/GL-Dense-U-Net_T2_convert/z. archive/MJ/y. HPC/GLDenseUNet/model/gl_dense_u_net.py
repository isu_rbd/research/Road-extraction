from keras import backend as K
from keras.models import Model
from keras.layers import Input, concatenate, multiply, Conv2D, MaxPooling2D, AveragePooling2D, GlobalAveragePooling2D, Activation, UpSampling2D, BatchNormalization, Reshape
from keras.optimizers import RMSprop
from keras.losses import binary_crossentropy

def dice_coeff(y_true, y_pred):
    smooth = 1.
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    score = (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
    return score

def dice_loss(y_true, y_pred):
    loss = 1 - dice_coeff(y_true, y_pred)
    return loss

def bce_dice_loss(y_true, y_pred):
    loss = binary_crossentropy(y_true, y_pred) + dice_loss(y_true, y_pred)
    return loss

def conv_block(x, growth_rate, name):
    
    ## Composition function: BN & ReLu Activation
    y = BatchNormalization(axis=3)(x)
    y = Activation('relu', name=name + '_0_relu')(y)
    
    ## Conv 1 x 1
    y = Conv2D( 4 * growth_rate, 1, use_bias=False, name=name+'_1_conv')(y)
    y = BatchNormalization(axis=3)(y)
    y = Activation('relu', name=name + '_1_relu')(y)
    
    ## Conv 3 x 3
    y = Conv2D( growth_rate, (3,3), padding='same', use_bias=False, name=name+'_2_conv')(y)
    
    ## Concat Input + Conv
    res = concatenate([x, y], axis=3)
    
    return res

def dense_block(x, n_blocks, growth_rate, name):
    
    for i in range(n_blocks):
        x = conv_block(x, growth_rate, name + '_block' + str(i+1))

    return x

def trans_block(x, reduction, name):
    
    #bn_axis = 3 if backend.image_data_format() == 'channels_last' else 1
    bn_axis = 3
    
    y = BatchNormalization(axis=3)(x)
    y = Activation('relu', name=name + '_relu')(y)
    y = Conv2D(int(K.int_shape(y)[bn_axis] * reduction), 1, use_bias=False, name=name+"_conv")(y)
    y = AveragePooling2D((2,2), strides=2, name=name+"_pool")(y)
    
    return y

def lau_block(x, name):
    
    bn_axis=3
    n_chan = K.int_shape(x)[bn_axis]
    n_half_chan = int( n_chan / 2 )
    
    y = BatchNormalization(axis=3)(x)
    y = Activation('relu', name=name + '_0_relu')(y)
    y1 = Conv2D( 3 * n_half_chan, 1, use_bias=False, name=name+"_conv1")(y)
    y3 = Conv2D( n_half_chan, (3,3), padding='same', use_bias=False, name=name+"_conv3")(y)
    y5 = Conv2D( n_half_chan, (5,5), padding='same', use_bias=False, name=name+"_conv5")(y)
    y7 = Conv2D( n_half_chan, (7,7), padding='same', use_bias=False, name=name+"_conv7")(y)
    y_con = concatenate([y3, y5, y7], axis=3)
    y_prod = multiply([y1, y_con])
    
    z = BatchNormalization(axis=3)(y_prod)
    z = Activation('relu', name=name + '_1_relu')(z)
    z = Conv2D( n_chan, 1, use_bias=False, name=name+"_conv1_final")(z)
    
    return z

def gau_block(x, n_blocks, growth_rate, n_target_chan, name):
    
    bn_axis=3
    wh_axis=1
    upsize = 2 * K.int_shape(x)[wh_axis]
    
    n_half_chan = int(n_target_chan / 2)
    n_chan_conv = n_half_chan - growth_rate * n_blocks
    assert n_chan_conv > 1
    
    y = BatchNormalization(axis=3)(x)
    y = Activation('relu', name=name + '_0_relu')(y)
    
    ## Conv 1 x 1
    y = Conv2D( n_chan_conv, 1, use_bias=False, name=name+"_conv1")(y) ## w x h x n_chan_conv
    y = BatchNormalization(axis=3)(y)
    y = Activation('relu', name=name + '_1_relu')(y) 
    
    ## Dense Block
    y = dense_block(y, n_blocks, growth_rate, name) ## w x h x n_half_chan
    y = BatchNormalization(axis=3)(y)
    y = Activation('relu', name=name + '_2_relu')(y) 
    
    ## Upper Deconvoluation: GAP
    gap = GlobalAveragePooling2D()(y)
    deconv1 = BatchNormalization()(gap)
    deconv1 = Activation('relu', name=name + '_deconv1_1_relu')(deconv1)
    deconv1 = Reshape((1,1,K.int_shape(gap)[1]))(deconv1)
    deconv1 = Conv2D( n_half_chan, 1, use_bias=False, name=name+"_deconv1_conv1")(deconv1)
    deconv1 = UpSampling2D((upsize, upsize))(deconv1) ## 2w x 2h x n_half_chan
    
    ## Lower Deconvolution
    deconv2 = UpSampling2D((2, 2))(y) ## 2w x 2h x n_half_chan
    
    ## Concatenation of upper and lower
    res = concatenate([deconv1, deconv2], axis=3) ## 2w x 2h x 2n_half_chan (= n_target_chan)
    
    return res
    
def get_gl_dense_unet(input_shape=(256, 256, 3),num_classes=1):
    
    channel0 = 16
    n_blocks = [ 1, 3, 5, 7, 10 ]
    growth_rate_down = 16
    growth_rate_up = 12
    
    ## Input Tensor 
    inputs = Input(shape=input_shape)
    ## 256 x 256 x 3
    
    ##### Contracting
    ## Conv 3 x 3 + DenseNet - 1
    down0 = Conv2D(channel0, (3, 3), padding='same')(inputs) ## 256 x 256 x 16
    
    ## DenseNet 1 
    down1 = dense_block(down0, n_blocks[0], growth_rate_down, "dense1") ## 256 x 256 x 32
    down1_pool = trans_block(down1, 1.0, "tran1") ## 128 x 128 x 32
    
    ## DenseNet 2
    down2 = dense_block(down1_pool, n_blocks[1], growth_rate_down, "dense2") ## 128 x 128 x 80
    down2_pool = trans_block(down2, 1.0, "tran2") ## 64 x 64 x 80
    
    ## DenseNet 3
    down3 = dense_block(down2_pool, n_blocks[2], growth_rate_down, "dense3") ## 64 x 64 x 160
    down3_pool = trans_block(down3, 1.0, "tran3") ## 32 x 32 x 160
    
    ## DenseNet 4
    down4 = dense_block(down3_pool, n_blocks[3], growth_rate_down, "dense4") ## 32 x 32 x 272
    down4_pool = trans_block(down4, 1.0, "tran4") ## 16 x 16 x 272
    
    ## DenseNet 5
    down5 = dense_block(down4_pool, n_blocks[4], growth_rate_down, "dense5") ## 16 x 16 x 432
    
    ##### Expanding
    bn_axis = 3
    ## GAU 1 <- LAU 1 (down5)
    lau1 = lau_block(down5, "lau1") ## 16 x 16 x 432
    
    ## Node 1 <- GAU 1 + LAU 2 (down4)
    gau1 = gau_block(lau1, n_blocks[3], growth_rate_up, K.int_shape(down4)[bn_axis],"gau1") ## 32 x 32 x 272
    lau2 = lau_block(down4, "lau2") ## 32 x 32 x 272
    node1 = concatenate([gau1, lau2], axis=bn_axis) ## 32 x 32 x (2 x 272)
    
    ## Node 2 <- GAU 2 (<- Node1) + LAU 3 (down3)
    gau2 = gau_block(node1, n_blocks[2], growth_rate_up, K.int_shape(down3)[bn_axis],"gau2") ## 64 x 64 x 160
    lau3 = lau_block(down3, "lau3") ## 64 x 64 x 160
    node2 = concatenate([gau2, lau3], axis=bn_axis) ## ## 64 x 64 x (2 x 160)
    
    ## Node 3 <- GAU 3 (<- Node2) + LAU 4 (down2)
    gau3 = gau_block(node2, n_blocks[1], growth_rate_up, K.int_shape(down2)[bn_axis],"gau3") ## 128 x 128 x 80
    lau4 = lau_block(down2, "lau4") ## 128 x 128 x 80
    node3 = concatenate([gau3, lau4], axis=bn_axis) ## ## 128 x 128 x (2 x 80)
    
    ## Node 4 <- GAU 4 (<- Node3) + down1
    gau4 = gau_block(node3, n_blocks[0], growth_rate_up, K.int_shape(down1)[bn_axis],"gau4") ## 256 x 256 x 32
    node4 = concatenate([gau4, down1], axis=bn_axis) ## 256 x 256 x ( 2 x 32 )
    print("node4", K.int_shape(node4))

    ## Activation function for the last node
    node4 = BatchNormalization(axis=3)(node4)
    node4 = Activation('relu')(node4)
    
    classifier = Conv2D(num_classes, (1, 1), activation='sigmoid')(node4)

    model = Model(inputs=inputs, outputs=classifier)

    model.compile(optimizer=RMSprop(lr=0.0001), loss=bce_dice_loss, metrics=[dice_coeff])

    return model
